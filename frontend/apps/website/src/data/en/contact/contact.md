---
type: contact
locale: en
hash: contact-arrival
  
contact:
  title: Everything for our guests! Any questions?
  content: |-
    You may well know that we are located in Tyrol’s beautiful Stubaital valley. But we haven’t yet told you the best way to get here. It doesn’t actually matter how you choose to arrive. Whether you come by car, in environmentally friendly style by train, or from far away by plane – the Stubaital is always amazingly close and well connected!
  request:
    path: /en/request
    text: Inquire now.
  link:
    url: https://goo.gl/maps/XgtTkn6SK3ARL9ZL7
    text: COME CLOSER
  directions:
    - title: By car
      content: |-
        Coming from the north via the A12 autobahn, take the A13 Brenner autobahn towards Italy. Immediately before the Schönberg toll station, exit the motorway into the beautiful Stubaital valley, then head for the "Stubaier Gletscher" [Stubai Glacier], towards the end of the valley through the village of Neustift. Approx. 6 km after Neustift you will reach the little village of Gasteig, where you turn left off the main road immediately after the Carve.In ski rental shop.
    - title: By train
      content: |-
        Travelling by train is an environmentally friendly, relaxing way to travel. The nearest railway station is at Innsbruck, just a 20-minute drive from the Gasteigerhof. We will gladly organise a transfer from the station to and from the hotel for you. You can of course also use public transport: a bus service runs practically to our front door from Innsbruck station.
    - title: By plane
      content: |-
        Are you coming from afar? Innsbruck Airport, just a 20-minute drive from the Gasteigerhof, is our gateway to the world. We will gladly organise an airport transfer to and from the hotel for you.
---
