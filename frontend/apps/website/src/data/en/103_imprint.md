---
type: page
locale: en
view: imprint
menu:
  - misc
rank: 103

path: /en/imprint
title: legal notice
meta:
  title: legal notice
  description: Here you can find our legal information.
---
## Responsible for the content:

Alpenwellnesshotel Gasteigerhof GmbH & CoKG \
Familie Siller \
Gasteig 42 \
6167 Neustift im Stubaital \
Österreich

**T** [+43 5226 2746](tel:+4352262746) \
**M** [info@gasteigerhof.at](mailto:info@gasteigerhof.at)

**CEO:** Matthias Siller \
**UID:** AT U61297545 \


## bank details:

**IBAN:** AT65 1600 0001 0052 6875 \
**BIC:** BTVAAT22 \
**Institute:** BTV – Bank für Tirol und Vorarlberg

## Design & Programming:

punkt. \
https://www.punkt.agency

  
