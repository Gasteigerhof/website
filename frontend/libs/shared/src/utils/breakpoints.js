import {
  breakpoint_xxs,
  breakpoint_xs,
  breakpoint_sm,
  breakpoint_md,
  breakpoint_lg,
  breakpoint_xl,
  breakpoint_xxl
} from "../styles/_variables.module.scss";

// parse SASS variables and convert rem to px: "1rem" -> 16
export const XXS = 16 * parseFloat(breakpoint_xxs);
export const XS = 16 * parseFloat(breakpoint_xs);
export const SM = 16 * parseFloat(breakpoint_sm);
export const MD = 16 * parseFloat(breakpoint_md);
export const LG = 16 * parseFloat(breakpoint_lg);
export const XL = 16 * parseFloat(breakpoint_xl);
export const XXL = 16 * parseFloat(breakpoint_xxl);
