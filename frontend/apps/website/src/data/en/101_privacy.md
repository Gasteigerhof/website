---
type: page
locale: en
view: privacy
menu:
  - misc
rank: 101

path: /en/privacy
title: Privacy
meta:
  title: privacy
  description: Privacy is also important to us on the Internet - we handle your data with care.
privacy:
  cookies:
    title: Cookies
    content: |-
      Here you can see all your Cookies and change settings.

---

## General Information on data processing

This Privacy Policy informs you about the scope, purpose and way of processing personal data as part of our online services and their related websites, functionalities and content. This Policy is valid independent of the domains, systems, platforms and devices (such as desktop computers or mobile devices) used to access these services.

Processed user data include general personal data (for example client names and addresses), contract data if applicable (for example services used, names of contact persons, payment details), usage data (for example the websites visited as part of our online services, interest in our products) and content data (for example data entered into our contact/enquiry form). We only process personal user data (this includes all data processing categories of our users) in keeping with relevant data protection provisions. This means that we only process data based on legal approval. This is justified, especially in cases where data processing is necessary or legally required to be able to fulfil our contractual obligations and deliver our online services, where the user has given his or her consent, and where we have a legitimate interest (interest in analyses, optimisation and economic activity as well as the security of our online services in accordance with GDPR Article 6, 1(f)).

## Organisational and technical security measures

We apply organisational, contractual and technical security measures according to latest technical standards to make sure that we comply with data protection laws and provisions and to protect the data we process against arbitrary or deliberate manipulation, loss, destruction or access by unauthorised parties.

## Data processing and establishing contact data

If you get in touch with us through a website form or email, the data you entered will be stored to be able to process your request and deal with follow-up questions. We will not pass these data on to other parties without your consent.

## Gathering fundamental access data

Based on our legitimate interest in accordance with GDPR Article 6, 1(f), we gather data about every access to the server that hosts the relevant service. Access data include the name of the accessed website, date and time of access, browser type and version, user operating system, referrer URL (the site visited before) and the host name of the user’s device (IP address). For security reasons, logfile data (such as for investigation purposes in cases of misuse or fraud) will be stored for a maximum period of 14 days and will then be deleted. Data that have to be stored for a longer period of time for reasons of evidence finding are exempt from deletion until the case has been solved completely.

## Cookies

Cookies are details that our webserver or webservers of third parties transfer to the web browser of the user, where they are stored for later use. They support basic website functionalities, improve the user experience and mark users for certain other purposes. Further information on the use of cookies can be found in the Cookie Information document on our website.

## Data processing and web analysis

Our website uses certain functionalities provided by Google Analytics, a web analysis service of Google Inc., (1600 Amphitheatre Parkway Mountain View, CA 94043, USA). Data processing is carried out in accordance with the legal provisions found in § 96 (3) of the Austrian Telecommunications Act as well as GDPR Article 6, 1(a) (consent) and / or (f) (legitimate interest). The privacy of our users is important to us. User data are therefore pseudonymised.

Google Analytics uses cookies. These are text-based data that are stored on your computer and allow for an analysis of your website use. The information and data generated are transferred and stored by Google in the United States. You can prevent this by changing your browser settings in a way that doesn’t allow cookies to be stored or by applying the relevant cookie settings. We concluded a relevant contract on commissioned data processing with the supplier.

Your IP address is gathered but will be pseudonymised immediately. This allows for an approximate localisation only. Google shortens your IP address within the European Union or other signatory states of the Agreement on the European Economic Area, which will anonymise your IP address. The last 8 bit of an IPv4 address are set to 0. In case of an IPv6 address, this will be done with the last 80 out of the 128 bits. User data are stored for a period of 14 months.

Google uses these data to analyse the usage of our website, to put together reports on website activity and to provide further services related to website and internet use. Google will also pass on these data to third parties if this is legally required or if these third parties are commissioned by Google to process these data. Based on their own accounts, Google will never link your IP address to other Google data. Here you can find Google’s current Privacy Policy: https://policies.google.com/privacy?hl=en.

## Your rights

You have the right to obtain information, to data correction, deletion, limitation and transfer as well as withdrawal or protest. Please contact us by sending a letter, fax or email.

If you believe that processing your data infringes on data protection laws or violates your legal data protection rights in a way, you can launch a complaint with the relevant regulatory authority. In Austria, this regulatory authority is the Datenschutzbehörde (Data Protection Authority).

## Privacy policy changes

We reserve the right to change this Privacy Policy in order to adjust it to amended legal provisions or if data processing procedures or the provided service have been changed. However, this only applies to data processing information. Should user consent be required or should parts of the Privacy Policy affect contractual provisions with users, changes will only be made with user consent.

(This is the English translation of the original »Datenschutz« document. In case of discrepancies, the German version applies.)
