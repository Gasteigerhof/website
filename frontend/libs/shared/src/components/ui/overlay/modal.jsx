import { useMemo, useRef, useEffect, useCallback } from "react";
import PropTypes from "prop-types";

import { CloseIcon } from "icons";
import { useKeyPress } from "hooks";

import * as styles from "./modal.module.scss";

export const Modal = ({
  id,
  header,
  footer,
  content,
  onClose,
  children,
  extraWrapperClassName,
  extraScrollerClassName,
  extraContainerClassName,
  extraContentClassName,
  extraCloseClassName,
  showCloseButton,
  closeOnBackdropClick,
  closeOnEscapeKeyPress,
}) => {
  const isEscapeKeyPressed = useKeyPress("Escape");
  const containerRef = useRef(null);
  const scrollerRef = useRef(null);
  const wrapperRef = useRef(null);

  const closeHandler = useCallback(() => {
    if (typeof onClose !== "function") {
      return;
    }

    onClose();
  }, [onClose]);

  const clickHandler = useCallback(
    (event) => {
      if (
        event.target !== containerRef.current &&
        event.target !== scrollerRef.current &&
        event.target !== wrapperRef.current
      ) {
        return;
      }

      if (!closeOnBackdropClick) {
        return;
      }

      closeHandler();
    },
    [wrapperRef, scrollerRef, closeOnBackdropClick, closeHandler]
  );

  const wrapperClassName = useMemo(
    () => [styles.wrapper, extraWrapperClassName].join(" "),
    [extraWrapperClassName]
  );

  const scrollerClassName = useMemo(
    () => [styles.scroller, extraScrollerClassName].join(" "),
    [extraScrollerClassName]
  );

  const containerClassName = useMemo(
    () => [styles.container, extraContainerClassName].join(" "),
    [extraContainerClassName]
  );

  const contentClassName = useMemo(
    () => [styles.content, extraContentClassName].join(" "),
    [extraContentClassName]
  );

  const closeClassName = useMemo(
    () => [styles.close, extraCloseClassName].join(" "),
    [extraCloseClassName]
  );

  const closeButton = useMemo(
    () =>
      showCloseButton && (
        <button className={styles.button} onClick={closeHandler}>
          <CloseIcon className={styles.svg} />
        </button>
      ),
    [showCloseButton, closeHandler]
  );

  useEffect(() => {
    if (!closeOnEscapeKeyPress) {
      return;
    }

    if (!isEscapeKeyPressed) {
      return;
    }

    closeHandler();
  }, [isEscapeKeyPressed, closeOnEscapeKeyPress, closeHandler]);

  return (
    <div id={id} ref={wrapperRef} onClick={clickHandler} className={wrapperClassName}>
      <div ref={scrollerRef} className={scrollerClassName}>
        <div ref={containerRef} className={containerClassName}>
          <div className={contentClassName}>
            <div className={closeClassName}>{closeButton}</div>
            {children}
          </div>
        </div>
      </div>
    </div>
  );
};

Modal.defaultProps = {
  id: null,
  extraWrapperClassName: "",
  extraScrollerClassName: "",
  extraContainerClassName: "",
  extraContentClassName: "",
  extraCloseClassName: "",
  showCloseButton: true,
  closeOnBackdropClick: true,
  closeOnEscapeKeyPress: true,
};

Modal.propTypes = {
  onClose: PropTypes.func.isRequired,
  id: PropTypes.string,
  extraWrapperClassName: PropTypes.string,
  extraScrollerClassName: PropTypes.string,
  extraContainerClassName: PropTypes.string,
  extraContentClassName: PropTypes.string,
  extraCloseClassName: PropTypes.string,
  showCloseButton: PropTypes.bool,
  closeOnBackdropClick: PropTypes.bool,
  closeOnEscapeKeyPress: PropTypes.bool,
};
