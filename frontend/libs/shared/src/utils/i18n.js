import { isClient } from "./misc";
import { getProperty } from "./object";

export const detectLanguage = () => {
  if (!isClient) {
    return null;
  }

  const { language, languages, userLanguage } = navigator;
  const detected = getProperty(languages, "0") || userLanguage || language;

  if (typeof detected !== "string") {
    return null;
  }

  return detected.substring(0, 2).toLowerCase();
};

export const extractLanguage = (locales) => {
  if (!isClient) {
    return null;
  }

  //
  // 1) `/(l1|l2|l3|...)/xxx`
  // 2) `/(l1|l2|l3|...)/`
  // 3) `/(l1|l2|l3|...)`
  //
  const group = `(${locales.join("|")})`;
  const path = window.location.pathname || "";
  const regex = new RegExp(`\\/${group}(?=\\/|$)`);

  const match = path.match(regex);
  if (match === null) {
    return null;
  }

  return getProperty(match, "1");
};
