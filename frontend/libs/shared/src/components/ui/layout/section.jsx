import { useMemo } from "react";
import PropTypes from "prop-types";

import * as styles from "./section.module.scss";

export const Section = ({
  className,
  color,
  align,
  size,
  noPaddingTop,
  noPaddingBottom,
  contentVisibility,
  ...props
}) => {
  noPaddingTop = size === null ? true : noPaddingTop;
  noPaddingBottom = size === null ? true : noPaddingBottom;

  const wrapperClassName = useMemo(
    () =>
      [
        styles.wrapper,
        styles[color],
        styles[size],
        styles[align],
        noPaddingTop ? styles.noPaddingTop : "",
        noPaddingBottom ? styles.noPaddingBottom : "",
        contentVisibility ? styles.contentVisibility : "",
        className,
      ].join(" "),
    [color, size, align, noPaddingTop, noPaddingBottom, contentVisibility, className]
  );

  return <section className={wrapperClassName} {...props} />;
};

Section.defaultProps = {
  contentVisibility: false,
  noPaddingBottom: false,
  noPaddingTop: false,
  color: "transparent",
  align: "left",
  size: "lg",
};

Section.propTypes = {
  size: PropTypes.oneOf(["sm", "md", "lg"]),
  align: PropTypes.oneOf(["left", "center", "right"]),
  color: PropTypes.oneOf(["transparent", "white", "black", "primary", "secondary", "gradient"]),
  noPaddingTop: PropTypes.bool,
  noPaddingBottom: PropTypes.bool,
  contentVisibility: PropTypes.bool,
};
