---
type: page
locale: en
view: hotel
menu:
  - main-left
rank: 2

path: /en/hotel
title: Hotel
meta:
  title: The Hotel | 4* Gasteigerhof Stubaital
  description: The Gasteigerhof is a real family affair. 45 rooms | Extensive range of culinary delights | spacious wellness area | Glacier view suites

hotel:
  intro:
    title: A family affair, with the Stubaital valley at its heart.
    content: |-
      The Gasteigerhof is truly a family affair. Built by father Hermann and mother Maria, who carefully and lovingly managed and expanded it over the decades, the Gasteigerhof is now being actively guided into the future by the second generation: Matthias, Patrizia and Adrian Siller are not however new to the business – they have known and loved it ever since they were children. You can see it too, in their dealings with their entire team, in the smiles that are constantly on their faces – and above all in the care they show their guests.

  about:
    title: A proven concept, done differently.
    content: |-
      The Gasteigerhof has meanwhile grown into a miniature institution. It is a hotel whose name is known far and wide beyond the little village of Gasteig and the Stubaital valley. This may be due to its proximity to the Stubai Glacier, the wonderful Stubai mountain pastures and hiking destinations, or the breathtaking natural attractions of the Stubai valley.
    image: ./hotel/about.jpg

  gallery:
    images:
      - ./hotel/gallerie-1.jpg
      - ./hotel/gallerie-2.jpg
      - ./hotel/gallerie-3.jpg
      - ./hotel/gallerie-4.jpg

  slogan:
    title: Every­thing only for our guests.
    banner: Around the clock.

  team:
    title: The finest things here and now.
    content: |-
      A little of this – and you will be able to confirm this after your stay – can also be credited to the Siller family and their long-standing team. And, in order to keep it that way, the Gasteigerhof is constantly evolving, while always remaining true to itself with genuine, honest hospitality.
    image: ./hotel/team.jpg

  kids:
    title: Children feel at home here with us.
    content: |-
      The Gasteigerhof in the Stubaital valley has always been a family hotel. It is not only family-run, but has always been family-friendly. Your children are most welcome to stay with us here, which is why we have set up our "Hamsterland" at the Gasteigerhof, with its ball pit, climbing wall, enjoyable games and much more besides. Children are also allowed to use our the indoor swimming pool with its volcano bubbler. Outdoors there is an adventure playground and, if you need special items in your room, please tell us at reception.
    image: ./hotel/kids.jpg

  offers:
    title: Our offerings
    items:
      - label: 45 rooms
        image: ./common/images/45-zimmer.svg
      - label: Restaurant
        image: ./common/images/restaurant.svg
      - label: indoor pool
        image: ./common/images/hallenbad.svg
      - label: breakfast
        image: ./common/images/fruehstueck.svg
      - label: 5-course menu
        image: ./common/images/5-gaenge-menue.svg
      - label: tasty snack
        image: ./common/images/jause.svg
      - label: Massage
        image: ./common/images/massage.svg
      - label: Sauna
        image: ./common/images/sauna.svg
      - label: Fitness
        image: ./common/images/fitness.svg
      - label: Kids-Area
        image: ./common/images/kids-area.svg
      - label: free wifi
        image: ./common/images/gratis-wlan.svg
      - label: glacier view
        image: ./common/images/gletscherblick.svg
      - label: waterfall
        image: ./common/images/wasserfall.svg
      - label: hiking
        image: ./common/images/wanderungen.svg
      - label: Ski-Rental
        image: ./common/images/skirent.svg
      - label: Skiing
        image: ./common/images/skifahren.svg
      - label: Crosscountry Skiing
        image: ./common/images/langlauf.svg        
---
