---
type: contact
locale: de
hash: kontakt-anreise
  
contact:
  title: Alles für den Gast. Noch Fragen?
  content: |-
    Dass wir uns im schönen Stubaital in Tirol befinden,
    wissen Sie vermutlich ja schon.

    Den besten Weg zu uns haben wir Ihnen aber noch nicht verraten. 
    Dabei ist es egal, wie Sie anreisen.

    Ob mit dem Auto, umweltfreundlich mit der Bahn oder von weit her 
    mit dem Flugzeug - das Stubaital ist immer erstaunlich nah und gut 
    angebunden!
  request:
    path: /anfrage
    text: Jetzt anfragen
  link:
    url: https://goo.gl/maps/XgtTkn6SK3ARL9ZL7
    text: Kommen Sie näher
  directions:
    - title: Auto
      content: |-
        Aus Norden kommend über die A12, auf die A13 Brennerautobahn Richtung Italien. 
        Unmittelbar vor der Mautstation Schönberg fahren Sie von der Autobahn ab in das 
        schöne Stubaital. Anschließend fahren Sie Richtung Stubaier Gletscher Richtung 
        Talende durch die Ortschaft Neustift. Ca. 6 km nach Neustift kommt das kleine 
        Örtchen Gasteig, wo Sie unmittelbar nach dem Skiverleih Carve.In links von der 
        Hauptstraße abbiegen.
    - title: Zug
      content: |-
        Umweltfreundlich und entspannt lässt es sich mit dem Zug anreisen. Der nächste 
        Bahnhof ist in Innsbruck. Lediglich 20 Auto-Minuten vom Gasteigerhof entfernt.
        Wir organisieren für Sie gerne einen Hoteltransfer vom Bahnhof zum Hotel und 
        wieder retour. Selbstverständlich können Sie auch komplett mit öffentlichen 
        Verkehrsmitteln anreisen. Vom Bahnhof Innsbruck fährt ein Bus bis fast vor 
        unsere Haustüre.
    - title: Flugzeug
      content: |-
        Sie kommen von weit her? Der Flughafen Innsbruck ist unser Tor zur Welt. Nur 20 
        Autominuten vom Gasteigerhof entfernt. Wir organisieren für Sie gerne einen 
        Flughafentransfer zum Hotel und zurück.
---
