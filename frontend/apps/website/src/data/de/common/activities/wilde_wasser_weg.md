---
type: page
locale: de
view: activity
  
path: /aktivitaeten-im-stubaital/wilde-wasser-weg
title: "Vom Gletscher bis ins Tal: Wilde-Wasser-Weg"
meta:
  title: "Vom Gletscher bis ins Tal: Wilde-Wasser-Weg"
  description: Wasser ist Leben! Und am Wilde-Wasser-Weg spürt man die gewaltige Kraft hautnah.

activity:
  date: 2022-06-19
  highlight: false
  hidden: false
  categories:
    - fruehling
    - sommer
    - herbst
    - sport
    - familie
  image: ./wilde-wasser-weg/image.jpg
  content: |-
    Wasser ist Leben! Und am Wilde-Wasser-Weg spürt man die gewaltige Kraft hautnah. In drei Etappen führt der Weg von Gletschern, über tosende Wasserfälle bis zu kleinen Auen. Perfekt für die ganze Familie und bei jeder Witterung.
  facts:
    - label: "Etappen"
      value: "3"
    - label: "geeignet für:"
      value: "ganze Familie"
    - label: "Etappe 1"
      value: "1,5h, 3,5km, 120hm"
    - label: "Etappe 2"
      value: "2,5h, 4km, 660hm"    
    - label: "Etappe 3"
      value: "1,5h, 3km, 400hm"            

  gallery:
    - ./wilde-wasser-weg/image.jpg
---
