---
type: meta
locale: de

meta:
  company: 4-Sterne Hotel Gasteigerhof

  contact:
    address:
      - Gasteig 42
      - 6167 Neustift
      - Österreich
    email:
      text: Email
      url: info@gasteigerhof.at
    phone:
      text: Telefon
      url: +43 5226 2746

  social_media:
    instagram: 
      text: Instagram 
      url: https://www.instagram.com
    facebook:
      text: Facebook
      url: https://www.facebook.com

  booking:
    text_small: Buchen
    text_large: Jetzt buchen
    url: https://buchen.gasteigerhof.at?skd-language-code=de

  request:
    text_small: Anfragen
    text_large: Jetzt anfragen
    path: /anfrage

  offers:
    label_included: Im Angebot enthalten

  activities:
    label_facts: Facts
    label_next: Nächstes Abenteuer

  form:
    status:
      sending: "Sende..."
    error:
      required: "Dieses Feld wird benötigt."

  params:
    category: kategorie
    offer: angebot
    page: seite

  empty_state:
    text: Für diese Kategorie gibt es momentan leider keine Einträge.
    label_button: Alle anzeigen

  navigation:
    label_more: Mehr
    label_next: Weiter
    label_prev: Zurück
    label_back_home: Zur Startseite
    label_back_overview: Zur Übersicht

  pagination:
    label_page: Seite

  error:
    title: Entschuldigen Sie bitte...
    content: |-
      ...aber diese Seite existiert nicht!

  cookies:
    video_text: Bitte akzeptieren Sie die notwendigen Cookies, um dieses Video wiederzugeben.
    video_button: Cookies akzeptieren
---
