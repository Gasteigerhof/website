import PropTypes from "prop-types";

import { Title, Section, Container, Markdown, getProperty, isProduction } from "@gh/shared";

export const Cookies = ({ data }) => {
  const title = getProperty(data, "title");
  const content = getProperty(data, "content");

  return (
    <Section id="cookies" size="md" noPaddingTop={true}>
      <Container>
        <Title wrapper="h1">{title}</Title>
        <Markdown content={content} />
        {isProduction && (
          <script
            async=""
            id="CookieDeclaration"
            src="https://consent.cookiebot.com/2ee59ba2-4758-46bb-a997-a342883dc631/cd.js"
            type="text/javascript"
          ></script>
        )}
      </Container>
    </Section>
  );
};

Cookies.defaultProps = {
  data: {},
};

Cookies.propTypes = {
  data: PropTypes.shape({
    title: PropTypes.string.isRequired,
  }).isRequired,
};
