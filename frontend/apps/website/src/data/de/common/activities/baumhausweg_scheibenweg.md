---
type: page
locale: de
view: activity

path: /aktivitaeten-im-stubaital/baumhausweg-scheibenweg
title: "Das Kinderparadies für Groß und Klein: Baumhaus- und Scheibenweg"
meta:
  title: "Das Kinderparadies für Groß und Klein: Baumhaus- und Scheibenweg"
  description:  Machen Sie die Wanderung durch die Stubaier Kalkkögel zum Erlebnis.

activity:
  date: 2022-06-22
  highlight: false
  hidden: false
  categories:
    - fruehling
    - sommer
    - herbst
    - sport
    - familie
    - kinder
  image: ./baumhausweg-scheibenweg/image.jpg
  content: |-
    Machen Sie die Wanderung durch die Stubaier Kalkkögel zum Erlebnis. Zahlreiche Baumhäuser laden zum Entdecken ein. Und beim Scheibenweg ist spielerische Spannung für die ganze Familie garantiert.
  facts:
    - label: "Dauer"
      value: "ca. 2 Stunden"
    - label: "geeignet für"
      value: "ganze Familie"
    - label: "Höhenunterschied"
      value: "100 Meter"
    - label: "Besonderheit"
      value: "zahlreiche Baumhäuser"     

  gallery:
    - ./baumhausweg-scheibenweg/image.jpg
---
