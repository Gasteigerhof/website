---
type: page
locale: en
view: cuisine
menu:
  - main-left
rank: 5

path: /en/cuisine
title: Cuisine
meta:
  title: Culinary | 4* Gasteigerhof Stubaital
  description: Daily gourmet breakfast with regional products, some from our own production | Afternoon snack with coffee and cake | Children's menu and ice cream buffet | 5-course dinner with regional delicacies

cuisine:
  intro:
    image: ./cuisine/intro.jpg
    title: Good and honest.
    content: |-
      It’s a common enough promise, which is a good thing – because the more who live up to this promise, the better it is for all of us. So we have to work all the harder to make sure you can still taste the difference at the Gasteigerhof.

  grid:
    images:
      top_left: ./cuisine/grid-top-left.jpg
      top_right: ./cuisine/grid-top-right.jpg
      bottom_left: ./cuisine/grid-bottom-left.jpg
      bottom_right: ./cuisine/grid-bottom-right.jpg
    texts:
      title: Ingredients and products at the Gasteigerhof.
      content: |-
        We are delighted to meet the demand for products of the highest quality, whether fresh bread from our local bakery or milk from the cattle that graze our Stubai mountain pastures in summer. Because – quite simply – being one step ahead for us means staying here in the region.

  services:
    title: The way to our hearts is through the stomach.
    labels:
      - Extensive breakfast buffet
      - Afternoon snack with regional produce
      - Homemade cakes with coffee and tea
      - 5-course dinner menu – vegetarian too, naturally
      - Accompanying salad and cheese buffet
      - Ice cream for dessert
      - Children’s menu
      - Special dietary needs met on request
      - Tea and fresh fruit in the wellness area
      - Chilled drinks and cocktails in the bar
      - Exquisite wine selection
---

