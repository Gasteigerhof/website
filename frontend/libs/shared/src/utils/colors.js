import { color_primary, color_secondary, color_tertiary } from "../styles/_variables.module.scss";

export const COLOR_PRIMARY = color_primary;
export const COLOR_SECONDARY = color_secondary;
export const COLOR_TERTIARY = color_tertiary;
