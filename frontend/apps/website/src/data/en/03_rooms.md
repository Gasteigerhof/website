---
type: page
locale: en
view: rooms
menu:
  - main-left
rank: 3

path: /en/rooms
title: Rooms
meta:
  title: The Rooms | 4* Gasteigerhof Stubaital
  description: Four room categories | double room | Family Room | Glacier View Suite | Suite Zuckerhütl | All rooms with balcony | Free WiFi | spacious bathroom

rooms:
  intro:
    title: Sleep well here at the Gasteigerhof.
    content: |-
      The 45 rooms at the Gasteigerhof are made for peace and relaxation. The four different categories are sure to provide the ideal room in which to stay, relax or retreat – or simply to sleep in late. By the way, all of our rooms have a private balcony.

  items:
    - id: doubleroom
      menu:
        title: double room
        subtitle: from
        price: "€ 100,-"
      info:
        title: Time for two.
        content: |-
          Our double rooms are fully equipped in alpine style and offer plenty of space. And, as you might expect at the Gasteigerhof, no two rooms are alike.
        lines:
          - label: Rates
            value: from € 95,-
          - label: Size
            value: 20 m²
          - label: Sleeps
            value: '2'
          - label: Special feature
            value: shower or bathtub, balcony and TV, safe
        images:
          - ./rooms/doppelzimmer.jpg
          - ./rooms/doppelzimmer.jpg
          - ./rooms/doppelzimmer.jpg
        url: ''

    - id: family room
      menu:
        title: family room
        subtitle: from
        price: "€ 105,-"
      info:
        title: A family affair.
        content: |-
          Are you coming as a family? We would be delighted! Our family rooms offer extra space and privacy for you and your child(ren). The separate living area means that there is also enough space for playing or for enjoying peace and quiet in the evening.
        lines:
          - label: Rates
            value: from € 100,-
          - label: Size
            value: 35 m²
          - label: Sleeps
            value: 2 - 4
          - label: Special feature
            value: living room, shower, balcony, HD-TV, safe
        images:
          - ./rooms/familienzimmer.jpg
          - ./rooms/familienzimmer.jpg
          - ./rooms/familienzimmer.jpg
        url: ''

    - id: suite glacier view
      menu:
        title: Suite Glacier View
        subtitle: from
        price: "€ 110,-"
      info:
        title: Glacier view.
        content: |-
          Would you like a little more? The Gletscherblick (Glacier View) Suite is beautifully outfitted with a real tiled stove, real wood floor, separate living room and large bathroom with bathtub. The sunny south-facing balcony with glacier views is of course included for your holiday fun.
        lines:
          - label: Rates
            value: ab € 110,-
          - label: Size
            value: 40 m²
          - label: Sleeps
            value: 2 - 4
          - label: Special feature
            value: south-facing balcony with views of glacier, living room, tiled stove, HD-TV, real wood, bathtub
        images:
          - ./rooms/suite-gletscherblick.jpg
          - ./rooms/suite-gletscherblick.jpg
          - ./rooms/suite-gletscherblick.jpg
        url: ''

    - id: suite-zuckerhuetl
      menu:
        title: Suite Zuckerhütl
        subtitle: from
        price: "€ 115,-"
      info:
        title: A room with more views.
        content: |-
          More sun, more mountains, more relaxation. The Zuckerhütl Suite offers an incredible feeling of space, with a separate living room, real tiled stove, wood floor and spacious bathroom with bathtub. The large, sunny south-facing balcony with glacier views is of course included.
        lines:
          - label: Rates
            value: ab € 115,-
          - label: Size
            value: 40 m²
          - label: Sleeps
            value: 2 - 4
          - label: Special feature
            value: south-facing balcony with glacier views, living room, tiled stove, HD-TV, real wood, shower
        images:
          - ./rooms/suite-zuckerhuetl.jpg
          - ./rooms/suite-zuckerhuetl.jpg
          - ./rooms/suite-zuckerhuetl.jpg
        url: ''

  pricelists:
    items: []
#      - file: ./rooms/pricelist-summer.pdf
#        text: Price sheet summer
#      - file: ./rooms/pricelist-winter.pdf
#        text: Price sheet winter

  annulation:
    title: Annulation conditions
    content: ''
    items:
      - label: up to 3 weeks
        value: no annulation fees
      - label: 3 to 1 week(s)
        value: 70% of total price
      - label: within last week
        value: 90% of total price
    link:
      text: More info
      path: /en/gtc/#annulation
---

