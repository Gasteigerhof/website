import { getProperty, InstagramIcon, FacebookIcon } from "@gh/shared";

import { useMeta } from "hooks";

import * as styles from "./social-media.module.scss";

export const SocialMedia = () => {
  const meta = useMeta();

  const instagram = getProperty(meta, "meta.social_media.instagram.url");
  const facebook = getProperty(meta, "meta.social_media.facebook.url");

  return (
    <div className={styles.wrapper}>
      <a href={instagram} className={styles.link} rel="noreferrer" target="_blank">
        <InstagramIcon className={styles.icon} />
      </a>
      <a href={facebook} className={styles.link} rel="noreferrer" target="_blank">
        <FacebookIcon className={styles.icon} />
      </a>
    </div>
  );
};
