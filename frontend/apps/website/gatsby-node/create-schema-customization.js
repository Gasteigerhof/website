exports.createSchemaCustomization = ({ actions: { createTypes } }) => {
  const typeDefs = `
    type MarkdownRemark implements Node {
      frontmatter: Frontmatter
    }
    type Frontmatter @infer {
      activity: Activity
      popup: Popup
      home: Home
      rooms: Rooms
    }
    type Activity @infer {
      video: Vimeo
      gallery: [File] @fileByRelativePath
      grid: ActivityGrid
    }
    type ActivityGrid @infer {
      texts: [String]
      images: [File] @fileByRelativePath
    }
    type Popup @infer {
      image: File @fileByRelativePath
    }
    type Home @infer {
      teaser: HomeTeaser
    }
    type Rooms @infer {
      pricelists: FileItems
    }
    type HomeTeaser @infer {
      videos: [File] @fileByRelativePath
      image: File @fileByRelativePath
    }
    type FileItems @infer {
      items: [FileItem]
    }
    type FileItem @infer {
      file: File @fileByRelativePath
      text: String
    }
    type Vimeo {
      id: String
    }
  `;

  createTypes(typeDefs);
};
