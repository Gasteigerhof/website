import { useCallback, useMemo, useEffect, useState, useContext } from "react";
import PropTypes from "prop-types";

import { ArrowIcon } from "icons";

import { Text } from "../typography";

import { AccordionContext } from "./accordion";

import * as styles from "./expandable.module.scss";

export const Expandable = ({ children, id, label, isOpened, autoClose, onClick }) => {
  const { openedId, toggleOpenedId } = useContext(AccordionContext);

  const [currentIsOpened, setCurrentIsOpened] = useState(isOpened);

  const wrapperclassName = useMemo(
    () => [styles.wrapper, currentIsOpened ? styles.active : ""].join(" "),
    [currentIsOpened]
  );

  const clickHandler = useCallback(() => {
    setCurrentIsOpened((oldIsOpened) => {
      const newIsOpened = !oldIsOpened;
      onClick(newIsOpened);
      return newIsOpened;
    });

    toggleOpenedId(id);
  }, [onClick, toggleOpenedId, id]);

  useEffect(() => {
    setCurrentIsOpened(isOpened);
  }, [isOpened]);

  useEffect(() => {
    if (!autoClose) {
      return;
    }
    setCurrentIsOpened(openedId === id);
  }, [autoClose, openedId, id]);

  return (
    <div className={wrapperclassName}>
      <button className={styles.button} onClick={clickHandler}>
        <Text size="lg" weight="bold" className={styles.label}>
          {label}
        </Text>
        <ArrowIcon className={styles.icon} />
      </button>
      {currentIsOpened && <div className={styles.content}>{children}</div>}
    </div>
  );
};

Expandable.defaultProps = {
  id: null,
  label: null,
  isOpened: false,
  autoClose: false,
  onClick: () => {},
};

Expandable.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  isOpened: PropTypes.bool,
  autoClose: PropTypes.bool,
  onClick: PropTypes.func,
};
