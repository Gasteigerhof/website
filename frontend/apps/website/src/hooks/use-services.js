import { useRef } from "react";
import { graphql, useStaticQuery } from "gatsby";

import { getProperty, useLocale } from "@gh/shared";

export const useServices = () => {
  const refServices = useRef(null);
  const refLocale = useRef(null);
  const { locale: currentLocale } = useLocale();
  const key = "allMarkdownRemark.edges";
  const results = useStaticQuery(query) || {};
  const edges = getProperty(results, key) || [];

  if (refServices.current === null || refLocale.current !== currentLocale) {
    const [edge] = edges
      .map((edge) => getProperty(edge, "node.frontmatter"))
      .filter(({ locale }) => currentLocale === locale);
    refServices.current = edge;
  }

  refLocale.current = currentLocale;

  return refServices.current;
};

const query = graphql`
  query useServicesQuery {
    allMarkdownRemark(filter: { frontmatter: { type: { eq: "services" } } }) {
      edges {
        node {
          frontmatter {
            locale
            services {
              title
              images {
                publicURL
              }
              labels
            }
          }
        }
      }
    }
  }
`;
