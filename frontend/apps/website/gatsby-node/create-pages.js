const path = require("path");

exports.createPages = async ({ actions: { createPage }, graphql, reporter }) => {
  let results;
  try {
    results = await graphql(
      `
        {
          allMarkdownRemark(filter: { frontmatter: { type: { eq: "page" } } }) {
            edges {
              node {
                id
                frontmatter {
                  path
                  view
                  locale
                }
              }
            }
          }
        }
      `
    );
  } catch (e) {
    reporter.error(`Error fetching pages`, e);
    return;
  }
  const {
    data: {
      allMarkdownRemark: { edges },
    },
  } = results;

  const createLocalizedPage = (component, pageContext) => {
    const { slug } = pageContext;

    reporter.info(`Creating page: ${slug}`);
    createPage({ path: slug, context: pageContext, component });
  };

  edges.forEach(({ node }) => {
    const {
      id,
      frontmatter: { path: slug, view: template, locale },
    } = node;
    const component = path.resolve(`src/templates/${template}.jsx`);

    createLocalizedPage(component, {
      id,
      slug,
      locale,
    });
  });
};
