import { useContext } from "react";

import { LocaleContext } from "contexts";

export const useLocale = () => useContext(LocaleContext);
