---
type: meta
locale: en

meta:
  company: Gasteigerhof Neustift Stubaital

  contact:
    address:
      - Gasteig 42
      - 6167 Neustift
      - Österreich
    email:
      text: Email
      url: info@gasteigerhof.at
    phone:
      text: Phone
      url: +43 5226 2746

  social_media:
    instagram: 
      text: Instagram 
      url: https://www.instagram.com
    facebook:
      text: Facebook
      url: https://www.facebook.com

  booking:
    text_small: Book
    text_large: Book now
    url: https://buchen.gasteigerhof.at?skd-language-code=en

  request:
    text_small: Request
    text_large: Request now
    path: /en/request

  offers:
    label_included: Included in offer

  activities:
    label_facts: Facts
    label_next: Next adventure

  form:
    status:
      sending: "Sending..."
    error:
      required: "This field is required."

  pagination:
    label_page: Page

  params:
    category: category
    offer: offer
    page: page

  empty_state:
    text: At the moment there are no entries for this category.
    label_button: Show all

  navigation:
    label_more: More
    label_next: Next
    label_prev: Back
    label_back_home: Back to home
    label_back_overview: Back to overview

  error:
    title: Sorry...
    content: |-
      ...but this page does not exist!
      
  cookies:
    video_text: Please accept the necessary cookies to play this video.
    video_button: Accept cookies
---
