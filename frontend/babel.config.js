const path = require("path");

/**
 * Top-level babel configuration for `transpile` from @gh/utils:
 * Submodules can consume it by adding ´--root-mode updward` flag.
 *
 * @see: https://babeljs.io/docs/en/7.5.0/config-files#monorepos
 */
module.exports = {
	presets: [
		["@babel/preset-env", { targets: "defaults" }],
		["@babel/preset-react", { runtime: "automatic" }],
	],
	plugins: [
		[
			"babel-plugin-module-resolver",
			{
				root: ["./src"],
			},
		],
		[
			"babel-plugin-inline-react-svg",
			{
				svgo: {
					multipass: true,
					plugins: [
						{
							name: "addAttributesToSVGElement",
							active: false,
						},
						{
							name: "addClassesToSVGElement",
							active: false,
						},
						{
							name: "inlineStyles",
							active: true,
						},
						{
							name: "cleanupAttrs",
							active: true,
						},
						{
							name: "cleanupEnableBackground",
							active: true,
						},
						{
							name: "cleanupIDs",
							active: false,
						},
						{
							name: "cleanupListOfValues",
							active: true,
						},
						{
							name: "cleanupNumericValues",
							active: true,
						},
						{
							name: "collapseGroups",
							active: true,
						},
						{
							name: "convertColors",
							active: true,
						},
						{
							name: "convertPathData",
							active: true,
						},
						{
							name: "convertShapeToPath",
							active: false,
						},
						{
							name: "convertStyleToAttrs",
							active: true,
						},
						{
							name: "convertTransform",
							active: true,
						},
						{
							name: "mergePaths",
							active: false,
						},
						{
							name: "minifyStyles",
							active: true,
						},
						{
							name: "moveElemsAttrsToGroup",
							active: true,
						},
						{
							name: "moveGroupAttrsToElems",
							active: true,
						},
						{
							name: "removeAttrs",
							active: false,
						},
						{
							name: "removeComments",
							active: true,
						},
						{
							name: "removeDesc",
							active: true,
						},
						{
							name: "removeDimensions",
							active: true,
						},
						{
							name: "removeDoctype",
							active: true,
						},
						{
							name: "removeEditorsNSData",
							active: true,
						},
						{
							name: "removeElementsByAttr",
							active: false,
						},
						{
							name: "removeEmptyAttrs",
							active: true,
						},
						{
							name: "removeEmptyContainers",
							active: true,
						},
						{
							name: "removeEmptyText",
							active: true,
						},
						{
							name: "removeHiddenElems",
							active: true,
						},
						{
							name: "removeMetadata",
							active: true,
						},
						{
							name: "removeNonInheritableGroupAttrs",
							active: true,
						},
						{
							name: "removeRasterImages",
							active: true,
						},
						{
							name: "removeStyleElement",
							active: true,
						},
						{
							name: "removeTitle",
							active: true,
						},
						{
							name: "removeUnknownsAndDefaults",
							active: true,
						},
						{
							name: "removeUnusedNS",
							active: true,
						},
						{
							name: "removeUselessDefs",
							active: true,
						},
						{
							name: "removeUselessStrokeAndFill",
							active: false,
						},
						{
							name: "removeViewBox",
							active: false,
						},
						{
							name: "removeXMLNS",
							active: false,
						},
						{
							name: "removeXMLProcInst",
							active: true,
						},
						{
							name: "sortAttrs",
							active: true,
						},
					],
					js2svg: {
						pretty: true,
						indent: "  ",
					},
				},
			},
		],
		["babel-plugin-syntax-dynamic-import"],
	],
};
