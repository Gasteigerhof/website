---
type: page
locale: en
view: apartments
menu:
  - main-left
rank: 4

path: /en/apartments
title: Apartments
meta:
  title: The Holiday Apartments | 4* Gasteigerhof Stubaital
  description: The Lärchenheim offers space for four families | Holiday apartments on the Stubai Glacier with their own kitchen and space for the whole family.

apartments:
  intro:
    title: More space for you.
    content: |-
      Our guests should have all the space they need for rest and relaxation. Some like to be cooked for, some like to cook undisturbed for themselves, while some prefer the atmosphere of the Gasteigerhof in the evening, sitting by the fire in the lounge. And some enjoy the peace and quiet and the chance to retreat offered to the whole family by their own holiday flat.
    images:
      - ./apartments/intro.jpg
      - ./apartments/grid-bottom-right.jpg
      - ./apartments/grid-top-right.jpg   
  grid:
    images:
      top_left: ./apartments/grid-top-left.jpg
      top_right: ./apartments/grid-top-right.jpg
      bottom_left: ./apartments/grid-bottom-left.jpg
      bottom_right: ./apartments/grid-bottom-right.jpg
    texts:
      title: Something for everyone.
      content: |-
        Because everyone has their own preferences, our guests can also spend their holidays in one of our four Lärchenheim holiday apartments as a personal retreat. And, if your day out skiing or hiking was that little bit too much, you can always come over to the Gasteigerhof, just a few steps away, and relax in the wellness area or enjoy dinner in our restaurant. The best of both worlds, you might say.
      banner: Enough space in our bathrooms.
  map:
    image: ./apartments/map.svg

  info:
    title: "Our holiday apartments:"
    content: |-
      Some prefer things a bit bigger, more private and a little quieter. With two separate double rooms per holiday flat, each with its own shower/WC, your family holiday is sure to be a success.
    lines:
      - label: Rates
        value: from € 85,-
      - label: Size
        value: 60 m²
      - label: Sleeps
        value: 2-4
      - label: Special feature
        value: HD-TV, quiet, sunny location, balcony with glacier views, free WI-FI, cot and highchairs, fully equipped kitchen, laundry service, safe.
    url: ''

  services:
    title: INCLUSIVE SERVICES FOR OUR GUESTS.
    labels:
      - Sauna and wellness area with indoor pool, brine grotto and much more besides.
      - Breakfast, snack and 5-course dinner menu on request
      - Massage, baths and cosmetic treatments on request
      - 5-course dinner menu – vegetarian too, naturally
      - Discounts available in the hotel’s own ski rental shop
---
