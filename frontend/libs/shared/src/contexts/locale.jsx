import { createContext, useState, useMemo, useCallback } from "react";
import PropTypes from "prop-types";

import { extractLanguage } from "utils";

const LOCALES = {
  de: true,
  en: false,
};

const validateLocale = (locale) => {
  locale = typeof locale !== "string" ? extractLanguage(ALL_LOCALES) : locale;
  return locale in LOCALES ? locale : DEFAULT_LOCALE;
};

const ALL_LOCALES = Object.keys(LOCALES);
const DEFAULT_LOCALE = (() => ALL_LOCALES.find((key) => !!LOCALES[key]))();

export const LocaleProvider = ({ locale: initialLocale, ...props }) => {
  const [locale, setLocale] = useState(validateLocale(initialLocale));
  const setLocaleValidated = useCallback((locale) => {
    setLocale(validateLocale(locale));
  }, []);

  const value = useMemo(
    () => ({
      locale,
      locales: ALL_LOCALES,
      setLocale: setLocaleValidated,
      defaultLocale: DEFAULT_LOCALE,
    }),
    [locale, setLocaleValidated]
  );

  return <LocaleContext.Provider value={value} {...props} />;
};

LocaleProvider.propTypes = {
  initialLocale: PropTypes.oneOf(ALL_LOCALES),
};

export const LocaleContext = createContext();
