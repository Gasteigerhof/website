---
type: page
locale: en
view: activity

path: /en/activities-in-stubai/waterfall-gasteig
title: "The jewel of the house: Gasteigerhof Waterfall"
meta:
  title: "The jewel of the house: Gasteigerhof Waterfall"
  description: Immediately behind our hotel there is a stunning water spectacle.

activity:
  date: 2022-06-20
  highlight: false
  hidden: false
  categories:
    - Spring
    - Summer
    - Fall
    - Gasteigerhof
    - Family
    - Children
  image: ./gasteig-wasserfall/image.jpg
  content: |-
    Immediately behind our hotel there is a stunning water spectacle. Get close to the Gasteigerhof waterfall on the adventure trail. You will be amazed by the spectacle.
  facts:
    - label: "walking time:"
      value: "10 Minutes"
    - label: "suitable for:"
      value: "the whole family"
    - label: "Height Waterfall"
      value: "70 Meters"

  video:
    id: 742812030
---
