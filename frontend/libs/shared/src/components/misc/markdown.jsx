import { useMemo } from "react";
import PropTypes from "prop-types";
import { navigate } from "gatsby";
import marked from "marked";

import { HTML } from "./html";

export const Markdown = ({ content, inline, ...props }) => {
  const html = useMemo(
    () =>
      typeof content === "string"
        ? inline
          ? marked.parseInline(content)
          : marked.parse(content)
        : null,
    [inline, content]
  );

  const clickHandler = (event) => {
    const { target } = event;

    switch (target.tagName) {
      case "A":
        const href = target.getAttribute("href");
        if (/^\/(?!\/)/.test(href)) {
          event.preventDefault();
          navigate(href);
        } else {
          event.preventDefault();
          window.open(href, "_blank", "noopener");
        }
        break;
      default:
    }
  };

  return <HTML onClick={clickHandler} content={html} {...props} />;
};

Markdown.defaultProps = {
  inline: false,
};

Markdown.propTypes = {
  content: PropTypes.node.isRequired,
  inline: PropTypes.bool.isRequired,
};
