---
type: popup
locale: de
  
popup:
  active: true
  title: Willkommen im Winter!

  content: |-
    Wir freuen uns auf Ihren Besuch. Nicht vergessen - Ski einpacken!

    ### Schon unsere Winter-Angebote entdeckt?

    Hier gehts zu den Angeboten: [Angebote Winter](https://gasteigerhof.at/angebote)

  image: ./01_Gasteigerhof_08_02_01_Winter22_23_RZ4.jpg
---