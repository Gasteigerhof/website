---
type: popup
locale: en
  
popup:
  active: true
  title: Welcome to Winter.
  content: |-
    We are looking forward to your visit. Don't forget to pack your skis!

    ### Already discovered our winter offers?

    Click here for the offers: [winter offers](https://gasteigerhof.at/en/offers)

  image: ./01_Gasteigerhof_08_02_01_Winter22_3_RZ4.jpg
---
