---
type: page
locale: en
view: activity

path: /en/activities-in-stubai/01_Gasteigerhof_15_01_Winter22:23_RZ.jpg
title: "Glacier on the doorstep: The Stubai Glacier"
meta:
  title: "Glacier on the doorstep: The Stubai Glacier"
  description: Stubai Glacier in the Stubaital. At the Gasteigerhof in front of the front door.

activity:
  date: 2022-06-18
  highlight: false
  hidden: false
  categories:
    - Winter
    - Fall
    - Family
    - Children
    - Sports
  image: ./stubaier-gletscher/image.jpg
  content: |-
    The Stubai Glacier is world famous. And on our doorstep. Snow is guaranteed from October to June, the shuttle bus runs several times an hour directly from the hotel to the glacier in just 10 minutes. And the skiing fun? unique. With 26 lifts and cable cars and 35 descents, nothing is left to be desired.
  facts:
    - label: "Special feature"
      value: "largest glacier ski area in the Alps"
    - label: "suitable for:"
      value: "the whole family"
    - label: "lift facilities"
      value: "26"
    - label: "open from-to"
      value: "Oktober until June"      

  gallery:
    - ./stubaier-gletscher/image.jpg
---
