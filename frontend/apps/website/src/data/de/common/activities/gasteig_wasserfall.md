---
type: page
locale: de
view: activity

path: /aktivitaeten-im-stubaital/gasteig-wasserfall
title: "Das Juwel des Hauses: Gasteigerhof Wasserfall"
meta:
  title: "Das Juwel des Hauses: Gasteigerhof Wasserfall"
  description: Der hauseigene Wasserfall ist vom Hotel aus in nur wenigen Minuten fußläufig erreichbar.

activity:
  date: 2022-06-20
  highlight: true
  hidden: false
  categories:
    - fruehling
    - sommer
    - herbst
    - gasteigerhof
    - familie
    - kinder
  image: ./gasteig-wasserfall/image.jpg
  content: |-
    Der hauseigene Wasserfall ist vom Hotel aus in nur wenigen Minuten fußläufig erreichbar. 
    Acimagnimus repratur, is esto que que viduciame sita et labori ipit quid quiducitem del 
    magnim delendae nos aspit que venditis aut ex et acepele ndunt, si occabo.
  facts:
    - label: "Gehzeit"
      value: "10 Minuten"
    - label: "geeignet für"
      value: "ganze Familie"
    - label: "Höhe Wasserfall"
      value: "70 Meter"

  video:
    id: 742812030
---
