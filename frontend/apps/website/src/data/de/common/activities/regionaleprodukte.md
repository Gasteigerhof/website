---
type: page
locale: de
view: activity

path: /aktivitaeten-im-stubaital/regionaleprodukte
title: "Einen Schritt weiterzugehen, heißt in der Region zu bleiben"
meta:
  title: "Einen Schritt weiterzugehen, heißt in der Region zu bleiben"
  description: Regionale Produkte, frisch auf den Tisch im Gasteigerhof Stubaital Tirol.

activity:
  date: 2022-10-10
  highlight: true
  hidden: false
  categories:
    - fruehling
    - sommer
    - herbst
    - winter
    - kulinarik
  image: ./regionaleprodukte/Gasteigerhof-Kulinarik-alm.jpg
  content: |-
    Sie haben richtig gelesen. Viele unserer Produkte kommen nicht nur aus der Region, sondern teilweise sogar aus der eigenen Haltung oder dem eigenen Anbau. Unser Schafkäse, der Honig, das ein oder andere Lamm und vieles weitere entstammt aus unserer eigenen Produktion. Wenn wir denn genug davon haben. So ist das nun mal, wenn die Dinge selbst hergestellt werden. Weil wir Ihnen aber so viele regionale Produkte wie möglich anbieten möchten, achten wir selbstverständlich auch darauf wo wir unsere Produkte beziehen, die nicht wir produzieren. Und das ist vor allem aus der Region. Bei Partnern, die wir kennen und seit Jahren vertrauen.
  facts:
    - label: "Durchschnittliche Entfernung, die ihr Essen zurücklegt, bevor es am Teller landet"
      value: "so wenig wie möglich"
    - label: "Anteil an Produkten aus unserer eigenen Produktion"
      value: "so viel wie möglich"
    - label: "Welche Produkte produzieren wir selbst"
      value: "Schafkäse, Eier, Honig, Lammfleisch und mehr"   

  gallery:
    - ./regionaleprodukte/Gasteigerhof-Kulinarik-alm.jpg
    - ./regionaleprodukte/Gasteigerhof-Kulinarik-Brot.jpg
    - ./regionaleprodukte/Gasteigerhof-Kulinarik-Kirchl.jpg
    - ./regionaleprodukte/Gasteigerhof-Kulinarik-Knoedel.jpg     
    - ./regionaleprodukte/Gasteigerhof-Kulinarik-Kaiserschmarren.jpg               
---
