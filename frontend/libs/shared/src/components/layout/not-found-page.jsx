import PropTypes from "prop-types";

import { Container, Section, Title, Text, LinkButtonArrowBack } from "../ui";
import { Markdown } from "../misc";

import * as styles from "./not-found-page.module.scss";

export const NotFoundPage = ({ title, text, button, backPath }) => (
  <Section size="lg" align="center">
    <Container>
      <Title wrapper="h1" size="s7">
        {title}
      </Title>
      <Text>
        <Markdown content={text} />
      </Text>
      <div className={styles.button}>
        <LinkButtonArrowBack to={backPath}>{button}</LinkButtonArrowBack>
      </div>
    </Container>
  </Section>
);

NotFoundPage.defaultProps = {
  title: null,
  text: null,
  button: null,
  backPath: "/"
};

NotFoundPage.propTypes = {
  title: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  button: PropTypes.string.isRequired,
};
