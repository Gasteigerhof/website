import { useRef } from "react";
import { graphql, useStaticQuery } from "gatsby";

import { getProperty, useLocale } from "@gh/shared";

export const useFooter = () => {
  const refFooter = useRef(null);
  const refLocale = useRef(null);
  const { locale: currentLocale } = useLocale();
  const key = "allMarkdownRemark.edges";
  const results = useStaticQuery(query) || {};
  const edges = getProperty(results, key) || [];

  if (refFooter.current === null || refLocale.current !== currentLocale) {
    const [edge] = edges
      .map((edge) => getProperty(edge, "node.frontmatter"))
      .filter(({ locale }) => currentLocale === locale);
    refFooter.current = edge;
  }

  refLocale.current = currentLocale;

  return refFooter.current;
};

const query = graphql`
  query useFooterQuery {
    allMarkdownRemark(filter: { frontmatter: { type: { eq: "footer" } } }) {
      edges {
        node {
          frontmatter {
            locale
            footer {
              intro
              acknowledgement
              copyright {
                text
              }
              disclaimer {
                text
                url
              }
            }
          }
        }
      }
    }
  }
`;
