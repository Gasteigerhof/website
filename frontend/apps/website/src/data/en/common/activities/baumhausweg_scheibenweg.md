---
type: page
locale: en
view: activity

path: /en/activities-in-stubai/treehouse-and-plate-trail
title: "The children's paradise for young and old: tree house and disc path"
meta:
  title: "The children's paradise for young and old: tree house and disc path"
  description: Take the walk through the Stubai Kalkkögel

activity:
  date: 2022-06-22
  highlight: false
  hidden: false
  categories:
    - Spring
    - Summer
    - Fall
    - Sports
    - Family
    - Children
  image: ./baumhausweg-scheibenweg/image.jpg
  content: |-
    Take the walk through the Stubai Kalkkögel - now that is an experience. The numerous tree houses on the way are just waiting to be explored. Fun and adventure for the whole family guaranteed on the so-called "disc trail".
  facts:
    - label: "Duration"
      value: "approx. 2 hours"
    - label: "suitable for:"
      value: "the whole family"
    - label: "altitude difference"
      value: "100 Meters"
    - label: "special feature"
      value: "numerous tree houses"     

  gallery:
    - ./baumhausweg-scheibenweg/image.jpg
---
