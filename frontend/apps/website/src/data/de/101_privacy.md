---
type: page
locale: de
view: privacy
menu:
  - misc
rank: 101

path: /datenschutz
title: Datenschutz
meta:
  title: Datenschutz
  description: Privatsphäre ist uns auch im Internet wichtig - wir gehen sorgsam mit Ihren Daten um.
---
Der Schutz Ihrer personenbezogenen Daten ist uns ein besonderes Anliegen. Ihre Daten werden von uns auf Basis der gesetzlichen Bestimmungen (Datenschutz-Grundverordnung Nr 2016/679 – im Folgenden "DS-GVO" sowie der nationalen Datenschutzvorschriften) verarbeitet.

## 1. Verantwortlicher
Verantwortlicher im Sinne von Art 4 Z 7 DS-GVO ist der Betreiber dieser Webseite gemäß den Angaben im Impressum (in dieser Datenschutzerklärung auch als "Wir", "Uns" bezeichnet). Dort finden Sie auch alle notwendigen Kontaktdaten zur Geltendmachung Ihrer Rechte (Punkt 6.).

## 2. Personenbezogene Daten
Wir verarbeiten lediglich jene personenbezogenen Daten, die Sie uns (durch Besuch unserer Webseite bzw durch die von Ihnen gemachten Angaben in einem Kontaktformular oder einer Newsletter-Anmeldung) zur Verfügung stellen, etwa: Vorname, Nachname, E-Mail-Adresse, Telefonnummer, Geburtsdatum/Alter, Geschlecht, Bankdaten (zB Kreditkartenummer, Kontonummer), IP-Adresse; wobei nicht in jedem Fall sämtliche der angegeben Datenarten betroffen sind oder angegeben werden müssen. Besondere Kategorien personenbezogener Daten im Sinne des Art 9 DS-GVO werden von uns im Rahmen dieser Webseite nicht verarbeitet.

## 3. Verarbeitung der personenbezogenen Daten
Die Verarbeitung Ihrer personenbezogenen Daten erfolgt in erster Linie zur Erfüllung eines Vertrages bzw zur Durchführung vorvertraglicher Maßnahmen (Art 6 Abs 1 lit b DS-GVO), da wir ohne diese Daten einen Vertrag mit Ihnen nicht vorbereiten, abschließen bzw erfüllen können. Weiters erfolgt die Verarbeitung zur Wahrung unserer berechtigten Interessen bzw berechtigten Interessen Dritter (Art 6 Abs 1 lit f DS-GVO), insbesondere auch zu Zwecken der Betriebssicherheit der Webseite, des Forderungsmanagements, des Direktmarketings in analoger und digitaler Form, der Bestandskundenwerbung, der statistischen Auswertung sowie der Verbesserung unseres Dienstleistungsangebots und dessen Qualität (zum Widerrufsrecht siehe Punkt 6.).

Wir werden Ihre personenbezogenen Daten lediglich in dem von der DS-GVO gedeckten Umfang verarbeiten. Eine Übermittlung Ihrer personenbezogenen Daten an Dritte erfolgt in der Regel nicht. Wenn – etwa zur Vertragserfüllung – eine Übermittlung an Dritte erforderlich sein sollte, wird diese Übermittlung im Einklang mit den Bestimmungen der DS-GVO erfolgen. Wir bitten Sie, in diesem Fall auch die Datenschutzerklärungen dieser Dritten zu beachten, da diese allenfalls zur Anwendung gelangen können. Ausgenommen hiervon ist die Nutzung für statistische und ähnliche Zwecke, hierzu werden jedoch keine personenbezogenen Daten, sondern ausschließlich anonymisierte Daten übermittelt, die eine Identifizierung einer natürlichen Person nicht erlauben und nicht einer spezifischen natürlichen Person zugeordnet werden können (siehe dazu auch Punkte 8, 9, 10).

## 4. Datensicherheit
Der Schutz Ihrer personenbezogenen Daten erfolgt durch organisatorische und technische Maßnahmen, wie etwa dem Schutz vor unberechtigtem Zugriff und technische Datensicherheitsvorkehrungen.
Bitte beachten Sie jedoch, dass ungeachtet dieser Bemühungen nicht gänzlich ausgeschlossen werden kann, dass Daten, welche durch Sie über das Internet bekannt gegeben werden, dennoch von anderen Personen eingesehen und potenziell genutzt werden könnten. Wir können deshalb keine Haftung oder Verantwortung für Fehler in der Datenübertragung oder den unautorisierten Zugriff durch Dritte (zB Hackerangriffe, Spyware, Malware etc) übernehmen.

## 5. Aufbewahrung personenbezogener Daten
Ihre personenbezogenen Daten werden von uns nur so lange aufbewahrt, wie dies zur Erfüllung vertraglicher bzw gesetzlicher Verpflichtungen notwendig ist (entsprechende Aufbewahrungspflichten können sich zB insbesondere aus steuerrechtlichen Vorschriften oder aus Vorschriften über die Produkthaftung ergeben). Ist diese Erforderlichkeit nicht mehr gegeben, werden die Daten gelöscht.

## 6. Auskunft, Löschung, Beschwerderecht
Vorbehaltlich des Bestehens allfälliger gesetzlicher Verschwiegenheitsverpflichtungen haben Sie das Recht, jederzeit Auskunft über Ihre gespeicherten personenbezogenen Daten, deren Herkunft, den Verarbeitungszweck sowie gegebenenfalls den Empfänger dieser Daten zu erhalten. Weiters haben Sie das Recht, die Berichtigung, Übertragung, Einschränkung der Bearbeitung, Sperrung oder Löschung Ihrer personenbezogenen Daten zu verlangen, wenn diese unrichtig sind oder die Grundlage für die Datenverarbeitung wegfällt. Ebenso haben Sie die Möglichkeit, Widerspruch gegen die Verarbeitung Ihrer personenbezogenen Daten auf Basis von Art 6 Abs 1 lit f DS-GVO ("Wahrung berechtigter Interessen") zu erklären.
Wir bitten Sie, sämtliche dieser Ansprüche an eine(n) der in unserem Impressum der Webseite bekannt gegebenen Kontaktmöglichkeit / Ansprechpartner zu richten.

Wir weisen darauf hin, dass ein allfälliger Widerspruch keinen Einfluss auf die Zulässigkeit der Verarbeitung personenbezogener Daten auf Basis sonstiger Erlaubnistatbestände nach Art 6 DS-GVO hat.
Sollten Sie der Ansicht sein, dass die Verarbeitung Ihrer personenbezogenen Daten gegen geltende datenschutzrechtliche Bestimmungen verstößt oder Ihre Ansprüche nach dem Datenschutzrecht in sonstiger Weise verletzt worden sind, haben sie das Recht, eine Beschwerde bei der zuständigen Aufsichtsbehörde (gemäß Art 77 DS-GVO) einzubringen.

## 7. Verwendung von Cookies
Um die Website nach Ihren Bedürfnissen zu gestalten und bestimmte Informationen zu sammeln, nutzen wir verschiedene Technologien, ua sogenannte Cookies. Cookies dienen dazu, die Internetnutzung und die Kommunikation zu vereinfachen.
Cookies sind kleine Textdateien, die unsere Website an Ihren Browser schickt und die auf Ihrem Computer abgelegt werden, um sie dort als eine anonyme Kennung zu speichern. Zweck dieser Cookies sind zB die bessere Steuerung der Verbindung während Ihres Besuchs auf unserer Webseite und eine effektivere Unterstützung, wenn Sie wieder auf unsere Website zurückkehren. Ohne diese befristete "Zwischenspeicherung" müssten bei einigen Anwendungen bereits getätigte Eingaben erneut erfolgen. Ein Cookie enthält nur die Daten, die ein Server ausgibt oder/und der Nutzer auf Anforderung eingibt (zB Aufbau: Angaben zu Domain, Pfad, Ablaufdatum, Cookiename und -wert). Cookies beinhalten also rein technische Informationen, keine personenbezogenen Daten. Cookies können auch nicht die Festplatte des Nutzers ausspähen, Schaden verursachen oder dergleichen.

Folgende Arten von Cookies können zum Einsatz gelangen:
- Session Cookies: Diese werden automatisch gelöscht, wenn der Internetbrowser geschlossen wird.
- Persistent Cookies: Diese bleiben für einen bestimmten Zeitraum auf Ihrem Computer gespeichert, die Dauer ist abhängig von dem hinterlegten Ablaufdatum.
- Third Party Cookies: Diese können insbesondere auch Cookies von diversen Social-Media-Plattformen (wie etwa Facebook, Twitter, You Tube, Instagram, Pinterest, Flickr etc) oder sonstigen Internetdiensleistern (wie etwa Google) beinhalten. Diese Cookies sammeln Informationen wie Verweildauer, Seitenaufrufe, Bewegung über Links etc. Sie werden etwa dazu genutzt, bestimmte Werbeinhalte einzublenden, die sich aus Suchverläufen, besuchten Webseiten und dergleichen ergeben. Diese Cookies können von uns nicht ausgelesen werden.
  Falls Sie es wünschen, können Sie eine Speicherung von Cookies (bzw bestimmter Arten von Cookies) über Ihren Web Browser generell unterdrücken oder entscheiden, ob Sie per Hinweis eine Speicherung wünschen oder nicht. Die Nichtannahme von Cookies kann jedoch zur Folge haben, dass einige Seiten nicht mehr richtig angezeigt werden oder die Nutzbarkeit eingeschränkt ist.

## 8. Analysetools
Die Webseite benutzt Google Analytics, einen Webanalysedienst der Google Inc. ("Google"). Google Analytics verwendet sog. "Cookies", Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Plattform durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Webseite werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert. Im Falle der Aktivierung der IP-Anonymisierung auf der Website/Plattform, wird Ihre IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Im Auftrag des Betreibers dieser Webseite wird Google diese Informationen benutzen, um Ihre Nutzung der Webseite/Plattform auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegenüber dem Websitebetreiber zu erbringen. Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt. Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen können. Sie können darüber hinaus die Erfassung der durch das Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem sie das unter diesem Link verfügbare Browser-Plugin herunterladen und installieren: https://tools.google.com/dlpage/gaoptout?hl=de
Google bietet an, die Erfassung durch Google Analytics zu verhindern. Bitte besuchen Sie dazu die entsprechende Internetspräsenz von Google.

## 9. Weitere Web-Tools
Auf dieser Webseite können unter Umständen weitere Web-Tools von Google zum Einsatz gelangen (etwa "Google AdWords", "Google AdSense", "Google AdExchange" oder "Google Double Klick for Publishers") – darüber, ob und in welchem Umfang diese Tools konkret eingesetzt werden, geben wir gerne Auskunft und ersuchen Sie, uns bei Fragen über eine der im Impressum der Webseite angegebene Kontaktmöglichkeit zu kontaktieren. Wir verarbeiten beim Einsatz dieser Tools keine personenbezogenen Daten von Ihnen, allenfalls erfolgt eine Datenverarbeitung jedoch durch Google selbst – bitte setzen Sie sich daher mit den entsprechenden Datenschutzhinweisen von Google auseinander um weitere Details der Nutzung Ihrer personenbezogenen Daten zu erlangen.

## 10. Änderungsvorbehalt
Wir müssen uns vorbehalten, die gegenständliche Datenschutzerklärung im Bedarfsfall anzupassen, etwa zur Abbildung von Rechtsentwicklungen im Bereich des Datenschutzes. Für die künftige Nutzung unserer Webseite gelten sodann die angepassten/geänderten Datenschutzbestimmungen. Wir empfehlen daher, diese von Zeit zu Zeit einzusehen.
