---
type: footer
locale: en
footer:
  intro: |-
    We are looking forward to meet you.
  acknowledgement: |-
    Yours, the Siller family.
  copyright:
    text: © Gasteigerhof
  disclaimer:
    text: Made by punkt.
    url: https://punkt.agency
---
