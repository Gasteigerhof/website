import { Fragment } from "react";
import { graphql } from "gatsby";

import { getProperty } from "@gh/shared";

import { Meta } from "./common/meta";
import { Intro } from "./cuisine/intro";
import { Grid } from "./cuisine/grid";
import { Services } from "./cuisine/services";

const Template = ({ data }) => {
  const meta = getProperty(data, "markdownRemark.frontmatter.meta");
  const intro = getProperty(data, "markdownRemark.frontmatter.cuisine.intro");
  const grid = getProperty(data, "markdownRemark.frontmatter.cuisine.grid");
  const services = getProperty(data, "markdownRemark.frontmatter.cuisine.services");

  return (
    <Fragment>
      <Meta data={meta} />
      <Intro data={intro} />
      <Grid data={grid} />
      <Services data={services} />
    </Fragment>
  );
};

export default Template;

export const query = graphql`
  query ($id: String!) {
    markdownRemark(id: { eq: $id }) {
      frontmatter {
        path
        title
        meta {
          title
          description
        }
        cuisine {
          intro {
            title
            content
            image {
              childImageSharp {
                gatsbyImageData(layout: FULL_WIDTH)
              }
            }
          }

          grid {
            images {
              top_left {
                childImageSharp {
                  gatsbyImageData(
                    width: 300
                    height: 400
                    layout: CONSTRAINED
                    transformOptions: { cropFocus: ENTROPY }
                  )
                }
              }
              top_right {
                childImageSharp {
                  gatsbyImageData(
                    width: 600
                    height: 400
                    layout: CONSTRAINED
                    transformOptions: { cropFocus: ENTROPY }
                  )
                }
              }
              bottom_left {
                childImageSharp {
                  gatsbyImageData(
                    width: 600
                    height: 400
                    layout: CONSTRAINED
                    transformOptions: { cropFocus: ENTROPY }
                  )
                }
              }
              bottom_right {
                childImageSharp {
                  gatsbyImageData(
                    width: 300
                    height: 400
                    layout: CONSTRAINED
                    transformOptions: { cropFocus: ENTROPY }
                  )
                }
              }
            }
            texts {
              title
              content
              banner
            }
          }

          services {
            title
            labels
          }
        }
      }
    }
  }
`;
