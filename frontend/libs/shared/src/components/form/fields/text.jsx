import { Input } from "./input";

export const TextField = ({ onChange, ...props }) => (
  <Input type="text" onChange={(event) => onChange(event.target.value)} {...props} />
);

TextField.propTypes = {
  ...Input.propTypes,
};
