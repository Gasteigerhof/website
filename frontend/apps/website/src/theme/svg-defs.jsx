import { COLOR_PRIMARY, COLOR_SECONDARY, COLOR_TERTIARY } from "@gh/shared";

import * as styles from "./svg-defs.module.scss";

export const SvgDefs = () => (
  <svg className={styles.svg}>
    <defs>
      <linearGradient id="gradient">
        <stop offset="0%" stopColor={COLOR_PRIMARY} />
        <stop offset="80%" stopColor={COLOR_TERTIARY} />
        <stop offset="100%" stopColor={COLOR_SECONDARY} />
      </linearGradient>
    </defs>
  </svg>
);
