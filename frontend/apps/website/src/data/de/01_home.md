---
type: page
locale: de
view: home
menu: null
rank: 1

path: /
title: Home
meta:
  title: 4* Gasteigerhof Hotel Stubaital
  description: Fühlen Sie sich angekommen im 4-Sterne Hotel Gasteigerhof in Neustift im Stubaital, Tirol. In unmittelbarer Nähe zum Stubaier Gletscher. Spezielle Familienangebote, Ski-Opening-Specials uvm. Mit großer Wellnessoase mit Hallenbad, Saunas, Outdoor-Bereich und Ruheräume sowie regionalem Kulinarik-Angebot.
  
home:
  teaser:
    logo:
      title: Gasteigerhof
      subtitle: Stubaital
    slogan: Fühlen Sie sich angekommen.
    content: |-
      Ein Urlaub im Stubaital ist Winter wie Sommer eine ausgesprochen gute Idee. 
      Denn zu jeder Jahreszeit lässt sich die Stubaier Berg- und Talwelt mit all 
      ihren Facetten erleben.


      Ob beim Skifahren am Gletscher oder beim Wandern auf herrlichen Almwiesen. 
      Unsere Aufgabe im Gasteigerhof ist es, seit Generationen, Ihnen diese Erfahrung 
      so schön wie möglich zu gestalten. So, dass Sie den Alltagsstress vergessen und 
      im Moment genießen. 
    image: ./home/winter.jpg
    videos: 
      - ./home/Gasteigerhof-winter-reel.mp4
      - ./home/Gasteigerhof-winter-reel.webm     

  intro:
    title: Willkommen im Gasteigerhof.
    content: |-
      Ein Urlaub im Stubaital ist Sommer wie Winter eine ausgesprochen gute Idee. 
      Denn zu jeder Jahreszeit lässt sich die Stubaier Berg- und Talwelt mit all 
      ihren Facetten erleben.


      Ob beim Wandern auf herrlichen Almwiesen oder beim Skifahren am Gletscher. 
      Unsere Aufgabe im Gasteigerhof ist es, seit Generationen, Ihnen diese Erfahrung 
      so schön wie möglich zu gestalten. So, dass Sie den Alltagsstress vergessen und 
      im Moment genießen. 
    images: 
      - ./home/intro-1.jpg
      - ./home/intro-2.jpg
      - ./home/intro-3.jpg
      - ./home/intro-4.jpg

  offers:
    title: Pauschalen und Angebote 
    subtitle: Sparen Sie bares Geld mit unseren Angeboten.
    content: Genießen Sie Ihren Urlaub im Gasteigerhof zu besonderen Konditionen. 

  management:
    greeting: Servus!
    title: Familiensache.
    content: |-
      Wir, die Familie Siller, bieten im Gasteigerhof seit Jahrzehnten ein Zuhause 
      für Menschen aus aller Welt.


      Für jene die Erholung suchen und bei uns für den Moment ein Zuhause finden.
    link:
      text: Das Hotel Gasteigerhof
      path: /hotel
    emblem: ./home/emblem.jpg
    persons:
      - name: XXX
        image: ./home/manager-1.jpg
      - name: XXX
        image: ./home/manager-2.jpg
      - name: XXX
        image: ./home/manager-3.jpg

  rooms:
    title: Zimmer mit Mehrblick.
    content: |-
      Mehr Natur, mehr Berge, mehr Sonne.

      Die 45 Zimmer des Hauses sind auf die Bedürfnisse unserer Gäste abgestimmt 
      und bieten zauberhaften Ausblick ins Stubaital.

      Ob Doppelzimmer, Familienzimmer oder exklusive Alpen-Suite.
    link:
      text: Sie haben die Wahl
      path: /zimmer
    image: ./home/rooms.jpg

  cuisine:
    title: '"Einen Schritt weiterzugehen, heißt bei uns in der Region zu bleiben."'
    content: |-
      "Um den Unterschied zu schmecken, muss man zwar weit gehen. Oft aber gar nicht 
      so weit schauen. Und deshalb verwenden wir so wo möglich lokal produzierte Lebensmittel."
    link:
      text: Zur Kulinarik
      path: /kulinarik
    banner: 'Auch beim Wein bleiben wir gerne innerhalb unserer Landesgrenzen.'
    image: ./home/Gasteigerhof_Stubaital_Speisesaal.jpg

  spa:
    title: Aus Zeit wird Auszeit.
    content: |-
      Wo Sie sich wohl befinden, wenn es Ihnen gut geht? Vermutlich in unserem Wellness-
      Bereich. Von A wie Almsauna bis Z wie Ziegenbutter-Creme-Bad. Dazwischen gibt's 
      selbstredend auch ein Schwimmbad.
    link:
      text: Zur Wellness-Oase
      path: /spa
    image: ./home/Gasteigerhof_Spa_6.jpg

  reviews:
    - name: Familie Müller, Deutschland
      text: '"Für uns ist die Gastfreundschaft einfach unvergleichbar."'
    - name: Cameron Sprent, Niederlande
      text: '"Ein toller Ort. Sehr freundliches und bemühtes Personal, leckeres Essen und perfekte Ausgangslage."'
    - name: Joanne Luychx, England
      text: '"Ein sehr liebliches Urlaubshotel. Das Frühstücksbuffet hat eine perfekte Auswahl, der 
        Spabereich ist sehr entspannend."'
    - name: High Sebination
      text: '"Tolles Hotel, sehr zentral, nettes Personal, gute Küche."'
    - name: Jürgen Christ
      text: '"Jedes Jahr gut. Gutes Essen, Gute Bar, renovierte Zimmer. Schöne Saune."'

  activities:
    title: Ein Tal der Erlebnisse
    content: |-
      Wir kennen unser Stubaital wie unsere Lodentasche. Deshalb möchten wir es nicht versäumen, 
      Ihnen ein paar Empfehlungen zu geben.
    link:
      text: Aktivitäten im Stubaital
      path: /aktivitaeten-im-stubaital
    images: 
      back: ./home/winter.jpg
      front: ./home/summer.jpg

  apartments:
    title: Mehr Privatsphäre gefällig?
    content: |-
      In unserem Lärchenheim finden Sie geräumige Ferienwohnungen, wo Sie auch mal selbst kochen 
      können. Müssen Sie aber nicht – der Gasteigerhof ist nur wenige Meter entfernt und empfängt 
      Sie gerne.
    link:
      text: Mehr erfahren
      path: /wohnungen
    image: ./home/apartments.jpg

---
