---
type: page
locale: en
view: activity

path: /en/activities-in-stubai/regionalproducts
title: "Going one step further means staying in the region"
meta:
  title: "Going one step further means staying in the region"
  description: Regional products, fresh on the table in the Gasteigerhof Stubaital Tirol.

activity:
  date: 2022-10-10
  highlight: false
  hidden: true
  categories:
    - Spring
    - Summer
    - Fall
    - Winter
    - Culinary
  image: ./regionaleprodukte/Gasteigerhof-Kulinarik-alm.jpg
  content: |-
    You read that right. Many of our products not only come from the region, but sometimes even from our own husbandry or cultivation. Our sheep's cheese, the honey, one or two lambs and much more comes from our own production. When we have enough of it. That's the way it is when you make things yourself. But because we want to offer you as many regional products as possible, we naturally also pay attention to where we source our products that we don't produce. And that is mainly from the region. With partners that we know and have trusted for years.
  facts:
    - label: "Average distance your food travels before it lands on your plate:"
      value: "as little as possible"
    - label: "Proportion of products from our own production:"
      value: "as much as possible"
    - label: "Which products do we produce ourselves:"
      value: "Sheep cheese, eggs, honey, lamb and more"   

  gallery:
    - ./regionaleprodukte/Gasteigerhof-Kulinarik-alm.jpg
    - ./regionaleprodukte/Gasteigerhof-Kulinarik-Brot.jpg
    - ./regionaleprodukte/Gasteigerhof-Kulinarik-Kirchl.jpg
    - ./regionaleprodukte/Gasteigerhof-Kulinarik-Knoedel.jpg     
    - ./regionaleprodukte/Gasteigerhof-Kulinarik-Kaiserschmarren.jpg               
---

