export const isValid = (value) => {
  if (typeof value !== "string") {
    return false;
  }

  return /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[\s0-9/.-]*$/.test(value);
};

export const toUri = (value) => {
  return isValid(value) ? `tel:${value.replace(/[^\d+]/g, "")}` : null;
};
