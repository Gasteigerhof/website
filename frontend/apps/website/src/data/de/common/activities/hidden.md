---
type: page
locale: de
view: activity

path: /aktivitaeten-im-stubaital/versteckt
title: Versteckte Aktivität
meta:
  title: XXX
  description: XXX

activity:
  date: 2022-10-01
  highlight: false
  hidden: true
  categories: []
  image: null
  content: |-
    Ich bin nicht in der Übersicht
  facts: []
---
