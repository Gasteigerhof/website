---
type: page
locale: en
view: activity

path: /en/activities-in-stubai/alpine-coaster-mieders
title: "Legendary and world-famous: Mieders summer toboggan run"
meta:
  title: "Legendary and world-famous: Mieders summer toboggan run"
  description: On the 2.8 km long summer toboggan run with 40 curves through the enchanting forests near Mieders

activity:
  date: 2022-06-23
  highlight: false
  hidden: false
  categories:
    - fruehling
    - sommer
    - herbst
    - sport
    - familie
    - kinder
  image: ./sommerrodelbahn-mieders/image.jpg
  content: |-
    Mieders summer toboggan run. Legendary, world-renowned and fun for the whole family. Experience a unique and exciting descent on the 2.8 km long summer toboggan run with 40 curves through the enchanting forests near Mieders. As fast or as leisurely as you like.
  facts:
    - label: "Length:"
      value: "2,8km"
    - label: "suitable for:"
      value: "the whole family"
    - label: "Difference in Altitude"
      value: "640 Meters"
    - label: "Top Speed"
      value: "up to 42km/h"      

  gallery:
    - ./sommerrodelbahn-mieders/image.jpg
---
