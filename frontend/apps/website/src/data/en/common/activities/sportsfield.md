---
type: page
locale: en
view: activity

path: /en/activities-in-stubai/sports-field
title: "Fun and games for our little ones"
meta:
  title: "Fun and games for our little ones"
  description: In-house multi-sports court for spring, summer and autumn

activity:
  date: 2023-09-13
  highlight: true
  hidden: false
  categories:
    - spring
    - summer
    - fall
    - gasteigerhof
    - sports
  image: ./sportplatz/Gasteigerhof-Sportplatz_06.jpg
  content: |-
    It was also a personal concern for us to open such a sports field at the house. Not only because we are incredibly happy when we can put a smile on the faces of our youngest guests and also provide some relaxation for their parents. No, we also have some children in the Siller family ourselves. And they are our biggest critics when it comes to designing a good playground and sports field. Fortunately, we have passed this test and now everyone, young and old, guest and family, can enjoy a new attraction right next to our house.

    The new sports field is accessible in spring, summer and autumn, depending on the weather.

  facts:
    - label: "suitable for"
      value: "our small and medium-sized guests in the house"
    - label: "Sports"
      value: "Football, Basketball, Field Hockey, Handball"
    - label: "Entertainment for the parents in the meantime"
      value: "Sun and cool drinks on the terrace in view"   

  gallery:
    - ./sportplatz/Gasteigerhof-Sportplatz_03.jpg
    - ./sportplatz/Gasteigerhof-Sportplatz_09.jpg
    - ./sportplatz/Gasteigerhof-Sportplatz_17.jpg
    - ./sportplatz/Gasteigerhof-Sportplatz_01.jpg
    - ./sportplatz/Gasteigerhof-Sportplatz_02.jpg
    - ./sportplatz/Gasteigerhof-Sportplatz_06.jpg
    - ./sportplatz/Gasteigerhof-Sportplatz_08.jpg
    - ./sportplatz/Gasteigerhof-Sportplatz_21.jpg      
---
