---
type: page
locale: de
view: imprint
menu:
  - misc
rank: 103

path: /impressum
title: Impressum
meta:
  title: Impressum
  description: Hier findest du alle gesetzlichen Informationen.
---
## Für den Inhalt verantwortlich

Alpenwellnesshotel Gasteigerhof GmbH & CoKG \
Familie Siller \
Gasteig 42 \
6167 Neustift im Stubaital \
Österreich

**T** [+43 5226 2746](tel:+4352262746) \
**M** [info@gasteigerhof.at](mailto:info@gasteigerhof.at)

**Geschäftsführung:** Matthias Siller \
**UID:** AT U61297545 \
**Aufsicht:** BH Innsbruck Land

## General terms and conditions
The general terms and conditions applicable to the Austrian hotel industry can be downloaded [here](https://www.hotelverband.at/down/agbh_061115.pdf).

## Concept, design & programming

punkt. \
https://www.punkt.agency
