---
type: page
locale: de
view: request
menu:
  - main-right
rank: 9

path: /anfrage
title: Anfrage
meta:
  title: Anfrage | 4* Gasteigerhof Stubaital
  description: Fragen Sie gerne bei uns an. Wir schicken Ihnen ehestmöglich ein unverbindliches Angebot für Ihren Urlaub im Gasteigerhof Stubaital.

request:
  intro:
    title: Ihre Anfrage
    content: |- 
      Senden Sie uns eine gerne eine Anfrage und Sie erhalten umgehend ein unverbindliches Angebot von uns!

  form:
    labels:
      retry: "Erneut senden"
      submit: "Jetzt anfragen"
      expander: "Vervollständigen Sie Ihre Daten"
    sections:
      - title: "Informationen zu Ihrem Aufenthalt"
        rows: 
          - fields:
            - name: arrival
              label: "Gewünschte Anreise"
              type: DATE
              required: true
            - name: departure
              label: "Gewünschte Abreise"
              type: DATE
              required: true
            - name: room_type
              label: "Zimmertyp"
              type: SELECT
              required: true
              options:
                - "Doppelzimmer"
                - "Familienzimmer"
                - "Suite Gletscherblick"
                - "Suite Zuckerhütl"
                - "Lärchenheim"
            expandable: false
          - fields:
            - name: adults
              label: "Anzahl Erwachsene"
              type: NUMBER
              required: true
            - name: children
              label: "Anzahl Kinder"
              type: NUMBER
              required: false
            - name: children_age
              label: "Alter Kinder"
              type: TEXT
              required: false
            expandable: false
      - title: "Ihre Kontaktdaten"
        rows:
          - fields:
            - name: title
              label: "Anrede"
              type: SELECT
              required: false
              options:
                - Herr
                - Frau
                - keine Angabe
                - Familie
                - Firma
            - name: forename
              label: "Vorname"
              type: TEXT
              required: false
            - name: surname
              label: "Nachname"
              type: TEXT
              required: false
            - name: email
              label: "Email"
              type: EMAIL
              required: true
            expandable: false
          - fields:
            - name: street
              label: "Straße"
              type: TEXT
              required: false
            - name: zip
              label: "PLZ"
              type: TEXT
              required: false
            - name: city
              label: "Ort"
              type: TEXT
              required: false
            expandable: true
          - fields:
            - name: country
              label: "Land"
              type: TEXT
              required: false
            - name: phone
              label: "Telefon"
              type: TEXT
              required: false
            expandable: true
      - title: null
        rows:
          - fields:
            - name: message
              label: "Weitere Anregungen und Wünsche"
              type: TEXTAREA
              required: false
            expandable: false
          - fields:
            - name: privacy
              label: "Ich habe die [Datenschutzerklärung](/datenschutz) gelesen"
              type: CHECKBOX
              required: true
            expandable: false
    success:
      title: Vielen Dank für Ihre Anfrage!
      content: |-
        Wir kümmern uns umgehend darum und treten mit Ihnen Kontakt.
    failure:
      title: Entschuldigen Sie bitte...
      content: |-
        ...aber beim Versand Ihrer Anfrage ist ein Fehler aufgetreten!
---
