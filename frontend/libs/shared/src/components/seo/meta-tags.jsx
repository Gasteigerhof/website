import { useMemo } from "react";
import PropTypes from "prop-types";
import { Helmet } from "react-helmet";
import { withPrefix } from "gatsby";

import { useLocale } from "hooks";

export const MetaTags = ({ title, description, page, pageLabel, companyName }) => {
  const { locale } = useLocale();

  const pageText = `${pageLabel} ${page}`;
  const metaDescription = description;
  const metaTitle = useMemo(() => {
    const titles = [title];

    if (page > 1) {
      titles.push(pageText);
    }

    titles.push(companyName);

    return titles.join(" | ");
  }, [title, page, pageText, companyName]);

  return (
    <Helmet>
      <html lang={locale} />
      <title>{metaTitle}</title>
      <meta name="robots" content="index, follow" />
      <meta name="description" content={metaDescription} />
      <meta name="og:locale" content={locale} />
      <meta name="og:type" content="website" />
      <meta name="og:title" content={metaTitle} />
      <meta name="og:description" content={metaDescription} />
      <meta name="twitter:title" content={metaTitle} />
      <meta name="twitter:description" content={metaDescription} />
      <meta name="format-detection" content="telephone=no" />
      {/* start: favicon.io */}
      <link rel="apple-touch-icon" sizes="180x180" href={withPrefix("/apple-touch-icon.png")} />
      <link rel="icon" type="image/png" sizes="32x32" href={withPrefix("/favicon-32x32.png")} />
      <link rel="icon" type="image/png" sizes="16x16" href={withPrefix("/favicon-16x16.png")} />
      <link rel="manifest" href={withPrefix("/site.webmanifest")} />
      <link rel="mask-icon" href={withPrefix("/safari-pinned-tab.svg")} color="#00a3a6" />
      <meta name="msapplication-TileColor" content="#000000" />
      <meta name="theme-color" content="#000000" />
      {/* end: favicon.io */}
    </Helmet>
  );
};

MetaTags.defaultProps = {
  page: 1,
  pageLabel: "Page",
  compayName: "Company",
};

MetaTags.propTypes = {
  page: PropTypes.number,
  pageLabel: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  companyName: PropTypes.string.isRequired,
};
