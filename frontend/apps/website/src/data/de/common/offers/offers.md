---
type: offers
locale: de

offers:

  items:
    - path: /angebote/#sonnenskilauf
      title: Sonnenskilauf im Hochwinter
      image: ./offer-skistart/Gasteigerhof-skistart-stubaiergletscher.jpg
      url: ''
      categories:
        - familie
        - winter
      summary:
        text: "7 Nächte bleiben, optional inkl. 6 Tage Gletscherskipass"
        price: ab € 910,- p.P. ohne Skipass, € 1.227,- p.P. mit Skipass
      details:
        text: "22.2. - 1.3.25: 7 Tage bleiben, nur 6 bezahlen."
        services:
          - "¾ VERWÖHNPENSION mit frischen Produkten aus der Region. Frühstücksbuffet, Nachmittagsjause, 5-Gang-Abendmenü mit Salatbuffet und Käse vom Brett"
          - "Benützung aller Freizeit- und Wellnessanlagen"
          - "Bademantel, Wellnesstücher und -taschen am Zimmer"
          - "Kostenloser Skibus direkt ab Hotel zum Stubaier Gletscher (ca. 8:50 Uhr & 10:22 Uhr) und retour (ca. 15:45 Uhr & 16:45 Uhr)"
          - "Unterhaltungsprogramm (Tanz- und Cocktailabend, Lichtbildervortrag)"
          - 'Für Kinder: "Hamsterland" mit Bällebad, Kletterwand, Tischfußball und Tischtennis'           

    - path: /angebote/#fruehjahrsskilauf
      title: Frühjahrsskilauf im Stubaital
      image: ./offer-sonnenskilauf/01_Gasteigerhof_08_02_01_Winter22_23_RZ3.jpg
      url: ''
      categories:
        - familie
        - winter
        - fruehling
      summary:
        text: "7 Nächte bleiben, optional inkl. 6 Tage Gletscherskipass"
        price: ab € 840,- p.P. ohne Skipass, € 1.157,- p.P. mit Skipass
      details:
        text: "15.3.-29.3.25, 7 Nächte bleiben, nur 6 bezahlen."
        services:
          - "¾ VERWÖHNPENSION mit frischen Produkten aus der Region. Frühstücksbuffet, Nachmittagsjause, 5-Gang-Abendmenü mit Salatbuffet und Käse vom Brett"
          - "Benützung aller Freizeit- und Wellnessanlagen"
          - "Bademantel, Wellnesstücher und -taschen am Zimmer"
          - "Kostenloser Skibus direkt ab Hotel zum Stubaier Gletscher (ca. 8:50 Uhr & 10:22 Uhr) und retour (ca. 15:45 Uhr & 16:45 Uhr)"
          - "Unterhaltungsprogramm (Tanz- und Cocktailabend, Lichtbildervortrag)"
          - 'Für Kinder: "Hamsterland" mit Bällebad, Kletterwand, Tischfußball und Tischtennis'    

    - path: /angebote/#sparwoche
      title: Sparwoche im Gasteigerhof
      image: ./offer-sonnenskilauf/offer-winter-gh.jpg
      url: ''
      categories:
        - familie
        - winter
        - fruehling
      summary:
        text: "7 Nächte bleiben, optional inkl. 6 Tage Gletscherskipass"
        price: ab € 700,- p.P. ohne Skipass, € 1.017,- p.P. mit Skipass
      details:
        text: "29.3.-5.4.25, 7 Nächte bleiben, nur 6 bezahlen."
        services:
          - "¾ VERWÖHNPENSION mit frischen Produkten aus der Region. Frühstücksbuffet, Nachmittagsjause, 5-Gang-Abendmenü mit Salatbuffet und Käse vom Brett"
          - "Benützung aller Freizeit- und Wellnessanlagen"
          - "Bademantel, Wellnesstücher und -taschen am Zimmer"
          - "Kostenloser Skibus direkt ab Hotel zum Stubaier Gletscher (ca. 8:50 Uhr & 10:22 Uhr) und retour (ca. 15:45 Uhr & 16:45 Uhr)"
          - "Unterhaltungsprogramm (Tanz- und Cocktailabend, Lichtbildervortrag)"
          - 'Für Kinder: "Hamsterland" mit Bällebad, Kletterwand, Tischfußball und Tischtennis'

  categories:
    - label: Alle
      value: null
    - label: Frühling
      value: fruehling
    - label: Sommer
      value: sommer
    - label: Herbst
      value: herbst
    - label: Winter
      value: winter
    - label: Familie
      value: familie

---

