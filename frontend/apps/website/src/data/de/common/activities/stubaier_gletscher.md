---
type: page
locale: de
view: activity

path: /aktivitaeten-im-stubaital/stubaier-gletscher
title: "Gletscher vor der Haustüre: Der Stubaier Gletscher"
meta:
  title: "Gletscher vor der Haustüre: Der Stubaier Gletscher"
  description: Stubaier Gletscher im Stubaital. Beim Gasteigerhof vor der Haustüre.

activity:
  date: 2022-06-18
  highlight: false
  hidden: false
  categories:
    - winter
    - herbst
    - familie
    - kinder
    - sport
  image: ./stubaier-gletscher/01_Gasteigerhof_15_01_Winter22:23_RZ.jpg
  content: |-
    Der Stubaier Gletscher ist weltbekannt. Und bei uns vor der Haustüre. Von Oktober bis Juni gibt es Schneegarantie, der Shuttle-Bus fährt täglich mehrmals die Stunde direkt ab Hotel in nur 10 Minuten zum Gletscher. Und das Skivergnügen? Einmalig. Mit 26 Lift- und Seilbahnanlagen und 35 Abfahrten bleiben keine Wünsche offen.
  facts:
    - label: "Besonderheit"
      value: "größtes Gletscherskigebiet der Alpen"
    - label: "geeignet für:"
      value: "ganze Familie"
    - label: "Liftanlagen"
      value: "26"
    - label: "geöffnet von-bis"
      value: "Oktober bis Juni"      

  gallery:
    - ./stubaier-gletscher/01_Gasteigerhof_15_01_Winter22:23_RZ.jpg
---
