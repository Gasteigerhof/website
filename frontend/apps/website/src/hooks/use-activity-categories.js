import { useRef } from "react";
import { graphql, useStaticQuery } from "gatsby";

import { getProperty, useLocale } from "@gh/shared";

export const useActivityCategories = () => {
  const refActivityCategories = useRef(null);
  const refLocale = useRef(null);
  const { locale: currentLocale } = useLocale();
  const key = "allMarkdownRemark.edges";
  const results = useStaticQuery(query) || {};
  const edges = getProperty(results, key) || [];

  if (refActivityCategories.current === null || refLocale.current !== currentLocale) {
    const [edge] = edges
      .map((edge) => getProperty(edge, "node.frontmatter"))
      .filter(({ locale }) => currentLocale === locale);
    refActivityCategories.current = edge;
  }

  refLocale.current = currentLocale;

  return refActivityCategories.current;
};

const query = graphql`
  query useActivityCategoriesQuery {
    allMarkdownRemark(filter: { frontmatter: { type: { eq: "activity-categories" } } }) {
      edges {
        node {
          frontmatter {
            locale
            activity_categories {
              items {
                label
                value
              }
            }
          }
        }
      }
    }
  }
`;
