export const isValid = (value) => {
  if (typeof value !== "string") {
    return false;
  }

  return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(value);
};

export const toUri = (value) => {
  return isValid(value) ? `mailto:${value}` : null;
};
