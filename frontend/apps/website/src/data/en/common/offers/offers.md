---
type: offers
locale: en

offers:

  items:
    - path: /angebote/#sunskiing
      title: Sun skiing in Winter
      image: ./offer-skistart/01_Gasteigerhof_08_02_01_Winter22_23_RZ4.jpg
      url: ''
      categories:
        - family
        - winter
      summary:
        text: "Stay 7 nights, optionally incl. 6-day glacier ski pass"
        price: from € 910,- p.p. without ski pass, € 1.227,- p.p. with ski pass
      details:
        text: "22.2. - 1.3.25: Stay 7 days, pay only 6."
        services:
          - "¾ TASTING BOARD with fresh products from the region. Breakfast buffet, afternoon snack, 5-course evening menu with salad buffet and cheese from the board"
          - "Use of all leisure and wellness facilities"
          - "Bathrobe, wellness towels and bags in the room"
          - "Free ski bus directly from the hotel to the Stubai Glacier (approx. 8:50 am & 10:22 am) and back (approx. 3:45 pm & 4:45 pm)"
          - "Entertainment programme (dance and cocktail evening, slide show)"
          - "For children “Hamsterland” with ball pool, climbing wall, table football and table tennis"

    - path: /angebote/#fruehjahrsskilauf
      title: Spring skiing in the Stubai Valley
      image: ./offer-sonnenskilauf/01_Gasteigerhof_08_02_01_Winter22_23_RZ3.jpg
      url: ''
      categories:
        - family
        - winter
        - spring
      summary:
        text: "Stay 7 nights, optionally incl. 6-day glacier ski pass"
        price: from € 840,- p.p. without ski pass, € 1.157,- p.p. with ski pass
      details:
        text: "15.3.-29.3.25, stay 7 nights, pay only 6."
        services:
          - "¾ TASTING BOARD with fresh products from the region. Breakfast buffet, afternoon snack, 5-course evening menu with salad buffet and cheese from the board"
          - "Use of all leisure and wellness facilities"
          - "Bathrobe, wellness towels and bags in the room"
          - "Free ski bus directly from the hotel to the Stubai Glacier (approx. 8:50 am & 10:22 am) and back (approx. 3:45 pm & 4:45 pm)"
          - "Entertainment programme (dance and cocktail evening, slide show)"
          - "For children “Hamsterland” with ball pool, climbing wall, table football and table tennis"

    - path: /angebote/#savingsweek
      title: Savings week at the Gasteigerhof
      image: ./offer-fruehjahrsskilauf/01_Gasteigerhof_08_02_01_Winter22_23_RZ.jpg
      url: ''
      categories:
        - family
        - winter
        - spring
      summary:
        text: "Stay 7 nights, optionally incl. 6-day glacier ski pass"
        price: from € 700,- p.p. without ski pass, € 1.017,- p.p. with ski pass
      details:
        text: "29.3.-5.4.25, stay 7 nights, pay only 6."
        services:
          - "¾ TASTING BOARD with fresh products from the region. Breakfast buffet, afternoon snack, 5-course evening menu with salad buffet and cheese from the board"
          - "Incl. one wellness massage (25 minutes)"
          - "Use of all leisure and wellness facilities"
          - "Bathrobe, wellness towels and bags in the room"
          - "Free ski bus directly from the hotel to the Stubai Glacier (approx. 8:50 am & 10:22 am) and back (approx. 3:45 pm & 4:45 pm)"
          - "Entertainment programme (dance and cocktail evening, slide show)"
          - "For children “Hamsterland” with ball pool, climbing wall, table football and table tennis"         

  categories:
    - label: Alle
      value: null
    - label: spring
      value: spring
    - label: summer
      value: summer
    - label: fall
      value: fall
    - label: winter
      value: winter
    - label: family
      value: family
---

