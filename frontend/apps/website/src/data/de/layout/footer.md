---
type: footer
locale: de
footer:
  intro: |-
    Wir freuen uns auf Sie.
  acknowledgement: |-
    Herzlich, \
    Ihre Familie Siller.
  copyright:
    text: © Gasteigerhof
  disclaimer:
    text: Made by punkt.
    url: https://punkt.agency
---
