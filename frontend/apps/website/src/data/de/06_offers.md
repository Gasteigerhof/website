---
type: page
locale: de
view: offers
menu:
  - main-right
rank: 6

path: /angebote
title: Angebote
meta:
  title: Angebote | 4* Gasteigerhof Stubaital
  description: Profitieren Sie von unseren ganzjährigen Sonderangeboten | Ski-Opening | Skistart | Frühjahrsskilauf | Sommerfrische | Goldener Herbst uvm. Zimmerpreise zu besonderen Konditionen inkl. gratis Gletscherskipass im Winter

offers:
  intro:
    title: Für Schnäppchenjäger.
    content: |-
      Ein Urlaub im Gasteigerhof zahlt sich immer aus. Vor allem, wenn Sie eines unserer Angebote buchen. 
      Ob allein, zu zweit oder mit der ganzen Familie. Für jede Jahreszeit gibt es die passende Pauschale. 
      Damit haben Sie stets alle Vorteile als Gast unseres Hauses – nur mehr. Für weniger.
---

