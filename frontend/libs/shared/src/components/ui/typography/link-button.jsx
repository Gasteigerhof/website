import PropTypes from "prop-types";
import { Link } from "gatsby";

import { removeProperty } from "utils";

import { ButtonStars, ButtonArrow, ButtonArrowBack } from "./button";

export const LinkButtonStars = (props) => <ButtonStars wrapper={Link} {...props} />;
LinkButtonStars.propTypes = {
  ...removeProperty(ButtonStars.propTypes),
  to: PropTypes.string.isRequired,
};

export const LinkButtonArrow = (props) => <ButtonArrow wrapper={Link} {...props} />;
LinkButtonArrow.propTypes = {
  ...removeProperty(ButtonArrow.propTypes),
  to: PropTypes.string.isRequired,
};

export const LinkButtonArrowBack = (props) => <ButtonArrowBack wrapper={Link} {...props} />;
LinkButtonArrowBack.propTypes = {
  ...removeProperty(ButtonArrowBack.propTypes),
  to: PropTypes.string.isRequired,
};
