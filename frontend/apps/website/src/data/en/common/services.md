---
type: services
locale: en

services:
  title: Inclusive services for our guests.
  images:
    - ./images/gratis-wlan.svg
    - ./images/hallenbad.svg
    - ./images/fruehstueck.svg
    - ./images/jause.svg
    - ./images/5-gaenge-menue.svg
    - ./images/kids-area.svg
    - ./images/bademaentel.svg
    - ./images/skiverleih.svg
  labels:
    - free Wi-Fi
    - wellness, pool & sauna
    - extensive breakfast buffet
    - afternoon snacks
    - 5-course dinner menu
    - large children’s play area
    - fresh bathrobes & towels
    - in-house ski rental
    - Free cancellation up to 3 weeks before arrival    
---

