---
type: page
locale: de
view: activity

path: /aktivitaeten-im-stubaital/sportplatz
title: "Spiel und Spaß für unsere Kleinen"
meta:
  title: "Spiel und Spaß für unsere Kleinen"
  description: Hauseigener Multisportplatz für Frühling, Sommer und Herbst

activity:
  date: 2023-09-13
  highlight: true
  hidden: false
  categories:
    - fruehling
    - sommer
    - herbst
    - gasteigerhof
    - sport
  image: ./sportplatz/Gasteigerhof-Sportplatz_06.jpg
  content: |-
    Dass wir so einen Sportplatz beim Haus eröffnen war uns auch ein persönliches Anliegen. Nicht nur, weil wir uns unglaublich freuen, wenn wir unseren jüngsten Gästen ein Lächeln ins Gesicht zaubern können und den dazugehörigen Eltern gleich auch etwas Entspannung verschaffen. Nein, wir haben ja auch selbst einige Kinder in der Familie Siller. Und die sind unsere größten KritikerInnen, wenn es um die Gestaltung eines guten Spiel- und Sportplatzes geht. Zum Glück haben wir diesen Test aber bestanden und nun können sich alle, egal ob Groß oder Klein, egal ob Gast oder Familie, über eine neue Attraktion, direkt bei unserem Haus freuen.

    Zugänglich ist der neue Sportplatz je nach Witterung im Frühling, Sommer und Herbst.

  facts:
    - label: "geeignet für"
      value: "unsere kleinen und mittelgroßen Gäste im Haus"
    - label: "Sportarten"
      value: "Fußball, Basketball, Feldhockey, Handball"
    - label: "Unterhaltung für die Eltern währenddessen"
      value: "Sonne und kühle Getränke auf der Terrasse in Sichtweite"   

  gallery:
    - ./sportplatz/Gasteigerhof-Sportplatz_03.jpg
    - ./sportplatz/Gasteigerhof-Sportplatz_09.jpg
    - ./sportplatz/Gasteigerhof-Sportplatz_17.jpg
    - ./sportplatz/Gasteigerhof-Sportplatz_01.jpg
    - ./sportplatz/Gasteigerhof-Sportplatz_02.jpg
    - ./sportplatz/Gasteigerhof-Sportplatz_06.jpg
    - ./sportplatz/Gasteigerhof-Sportplatz_08.jpg
    - ./sportplatz/Gasteigerhof-Sportplatz_21.jpg      
---
