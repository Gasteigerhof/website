---
type: page
locale: en
view: home
menu: null
rank: 1

path: /en
title: Home
meta:
  title: 4* Gasteigerhof Hotel Stubai Valley | Feel at home
  description: Feel at home in the 4-star Hotel Gasteigerhof in Neustift in the Stubai Valley, Tyrol. In the immediate vicinity of the Stubai Glacier. Special family offers, ski opening specials and much more. With a large wellness oasis with indoor pool, saunas, outdoor area and relaxation rooms as well as regional culinary offerings.
  
home:
  teaser:
    logo:
      title: Gasteigerhof
      subtitle: Stubaital
    slogan: Arrive and feel at home.
    content: |-
      A holiday in the Stubaital valley is an excellent choice, in winter or in summer, because you can experience the variety of the Stubai mountains and valleys at any time of the year. Enjoy skiing on the glacier or hiking over beautiful mountain meadows. Our task at the Gasteigerhof has for generations been to ensure that your holiday experience is as wonderful as possible, so you can forget the stress of everyday life and enjoy the moment.
    image: ./home/winter.jpg
    videos:
      - ./home/gasteigerhof-winter-reel.mp4
      - ./home/gasteigerhof-winter-reel.webm    

  intro:
    title: Welcome to the Gasteigerhof.
    content: |-
      A holiday in the Stubaital valley is an excellent choice, in winter or in summer, because you can experience the variety of the Stubai mountains and valleys at any time of the year. Enjoy skiing on the glacier or hiking over beautiful mountain meadows. Our task at the Gasteigerhof has for generations been to ensure that your holiday experience is as wonderful as possible, so you can forget the stress of everyday life and enjoy the moment. 
    images: 
      - ./home/intro-1.jpg
      - ./home/intro-2.jpg
      - ./home/intro-3.jpg
      - ./home/intro-4.jpg

  offers:
    title: Packages and offers
    subtitle: Save money with our offers.
    content: Enjoy your holiday at the Gasteigerhof at special conditions.

  management:
    greeting: Servus!
    title: A family affair.
    content: |-
      We, the Siller family, have for decades been playing host at the Gasteigerhof to visitors from all over the world.


      For those seeking relaxation and looking for a place to savour the moment with us.
    link:
      text: The Hotel Gasteigerhof
      path: /en/hotel
    emblem: ./home/emblem.jpg
    persons:
      - name: XXX
        image: ./home/manager-1.jpg
      - name: XXX
        image: ./home/manager-2.jpg
      - name: XXX
        image: ./home/manager-3.jpg

  rooms:
    title: A room with more views.
    content: |-
      The 45 rooms of our establishment are tailored to the needs of our guests and offer enchanting views of the Stubaital valley. Whether a double room, family room or an exclusive alpine suite.
    link:
      text: The Choice is yours
      path: /en/rooms
    image: ./home/rooms.jpg

  cuisine:
    title: '"Going one step further means staying with us here in the region."'
    content: |-
      "You have to go a long way to taste the difference. But often you don't look that far. And that's why we use locally produced food wherever possible."
    link:
      text: TO OUR CUISINE
      path: /en/culinary
    banner: 'We also like to stay within our national borders when it comes to wine.'
    image: ./home/Gasteigerhof_Stubaital_Speisesaal.jpg

  spa:
    title: Time becomes time out.
    content: |-
      Where do you go to when you are feeling well? Perhaps to our wellness area. From A for alpine sauna to Z for zero kilometres. And in between, of course, there’s also a swimming pool.
    link:
      text: TO THE WELLNESS OASIS
      path: /en/spa
    image: ./home/Gasteigerhof_Spa_6.jpg

  reviews:
    - name: Müller Family, Germany
      text: '"In our view the hospitality is simply incomparable."'
    - name: Cameron Sprent, Netherlands
      text: '"A great place. Very friendly and caring staff, delicious food and an ideal starting point."'
    - name: Joanne Luychx, England
      text: '"A really lovely holiday hotel. The breakfast buffet offers the perfect selection, while the spa area is very relaxing."'
    - name: High Sebination
      text: '"Great hotel, very central, nice staff, good cooking."'
    - name: Jürgen Christ
      text: '"Good each year we come. Good food, good bar, renovated rooms. Nice saunas."'

  activities:
    title: A VALLEY FULL OF ADVENTURE
    content: |-
      We know the Stubaital valley like the back of our hands. That’s why we want to take the opportunity to make a few recommendations.
    link:
      text: ACTIVITIES IN THE STUBAITAL VALLEY
      path: /en/activities-in-stubai
    images: 
      back: ./home/winter.jpg
      front: ./home/summer.jpg

  apartments:
    title: WOULD YOU LIKE MORE PRIVACY?
    content: |-
      Our Lärchenheim apartments offer spacious holiday apartments where you can also cook for yourself. But you don’t have to – the Gasteigerhof is just a few steps away and we will be happy to welcome you.
    link:
      text: LEARN MORE
      path: /en/apartments
    image: ./home/apartments.jpg

---
