const { createPages } = require("./gatsby-node/create-pages");
const { onCreateWebpackConfig } = require("./gatsby-node/on-create-webpack-config");
const { createSchemaCustomization } = require("./gatsby-node/create-schema-customization");

exports.createPages = createPages;

exports.onCreateWebpackConfig = onCreateWebpackConfig;
exports.createSchemaCustomization = createSchemaCustomization;
