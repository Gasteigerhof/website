import PropTypes from "prop-types";

export const HTML = ({ content, wrapper: Wrapper, ...props }) => (
  <Wrapper
    {...props}
    dangerouslySetInnerHTML={{
      __html: content,
    }}
  />
);

HTML.defaultProps = {
  wrapper: "div",
};

HTML.propTypes = {
  wrapper: PropTypes.string,
  content: PropTypes.node.isRequired,
};
