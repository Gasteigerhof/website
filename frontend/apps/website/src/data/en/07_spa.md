---
type: page
locale: en
view: spa
menu:
  - main-right
rank: 7

path: /en/spa
title: Spa
meta:
  title: Spa | 4* Gasteigerhof Stubaital
  description: Spacious wellness oasis with indoor pool | Large range of saunas | rest rooms | Baths | Massages | Cosmetics | Nature relaxation at the waterfall | The Gasteigerhof is a retreat for the whole family

spa:
  intro:
    title: Wellness that goes further.
    content: |-
      There are not many times during the year when you can just let yourself drift. We think your stay at the Gasteigerhof should be one of those times. Because even holidays can sometimes be over-planned. Get up early, then ski or hike all day, day in and day out. So you should not miss out on time spent in our wellness area, so as to turn your time here into a real time out.
    images:
      - ./spa/intro.jpg
      - ./spa/Gasteigerhof_Spa.jpg
      - ./spa/Gasteigerhof_Spa_1.jpg
      - ./spa/Gasteigerhof_Spa_2.jpg
      - ./spa/Gasteigerhof_Spa_3.jpg
      - ./spa/Gasteigerhof_Spa_4.jpg
      - ./spa/Gasteigerhof_Spa_5.jpg  
      - ./spa/Gasteigerhof_Spa_6.jpg
      - ./spa/Gasteigerhof_Spa_7.jpg
      - ./spa/Gasteigerhof_Spa_8.jpg
      - ./spa/Gasteigerhof_Spa_9.jpg
      - ./spa/Gasteigerhof_Spa_10.jpg
      - ./spa/Gasteigerhof_Spa_11.jpg
      - ./spa/Gasteigerhof_Spa_12.jpg
      - ./spa/Gasteigerhof_Spa_13.jpg

  services:
    title: WELLNESS-INCLUSIVE SERVICES FOR OUR GUESTS.
    images:
      - ./common/images/hallenbad.svg
      - ./common/images/massage.svg
      - ./common/images/sauna.svg
    labels:
      - "Alpine sauna – bio sauna – steam bath"
      - "Relaxation rooms – vitality centre – brine grotto – gymnasium"
      - "Relax cabin – hay beds – spring-water fountain"
      - "Colour shower – indoor swimming pool – sunbathing lawns"
      - "Vitalbar"
  grid:
    images:
      top_left: ./spa/Gasteigerhof_Spa_1.jpg
      top_right: ./spa/Gasteigerhof_Spa_6.jpg
      bottom_left: ./spa/Gasteigerhof_Spa_10.jpg
      bottom_right: ./spa/Gasteigerhof_Spa_13.jpg
    texts:
      title: And, while you’re here…
      content: |-
        For total relaxation, we recommend our massages, baths and beauty treatments. Come and be treated by our highly trained staff! You can book spontaneously at reception or online in advance. Booking online means you benefit from our early booking discounts.
      banner: We’ll make you relax!
  
  offers:
    title: Relaxation is in our nature.
    content: |-
      This nature can be found right outside our door, where you can truly switch off – whether in winter, on a quiet hike through the snowy Stubai landscape, or in summer, lying on a wooden lounger with the pleasant sound of the waterfall in the background. Please ask at reception about special relaxation tips for the surrounding area.
    image: ./spa/natur.jpg
    items:
      - label: THE ENTIRE OFFER AT A GLANCE
        file: ./spa/gasteigerhof-wellness-angebot.pdf
---
