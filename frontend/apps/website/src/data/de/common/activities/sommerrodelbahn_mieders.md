---
type: page
locale: de
view: activity

path: /aktivitaeten-im-stubaital/sommerrodelbahn-mieders
title: "Legendär und weltbekannt: Sommerrodelbahn Mieders"
meta:
  title: "Legendär und weltbekannt: Sommerrodelbahn Mieders"
  description: Sommerrodelbahn Mieders. Legendär, weltbekannt und ein Spaß für die ganze Familie.

activity:
  date: 2022-06-23
  highlight: false
  hidden: false
  categories:
    - fruehling
    - sommer
    - herbst
    - sport
    - familie
    - kinder
  image: ./sommerrodelbahn-mieders/image.jpg
  content: |-
    Sommerrodelbahn Mieders. Legendär, weltbekannt und ein Spaß für die ganze Familie. Auf der 2,8 Kilometer langen Sommerrodelbahn in 40 Kurven durch die zauberhaften Wälder bei Mieders erleben Sie eine einmalige und spannende Abfahrt. So rasant oder gemütlich Sie möchten.
  facts:
    - label: "Länge:"
      value: "2,8km"
    - label: "geeignet für:"
      value: "ganze Familie"
    - label: "Höhenunterschied"
      value: "640 Meter"
    - label: "Höchstgeschwindigkeiten"
      value: "bis zu 42km/h"      

  gallery:
    - ./sommerrodelbahn-mieders/image.jpg
---
