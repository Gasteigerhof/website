---
type: activity-categories
locale: en

activity_categories:
  items:
    - label: Alle
      value: null
    - label: Spring
      value: Spring
    - label: Summer
      value: Summer
    - label: Fall
      value: Fall
    - label: Winter
      value: Winter
    - label: Family
      value: Family
    - label: Gasteigerhof
      value: gasteigerhof
    - label: Village life
      value: Village life
    - label: Children
      value: Children
    - label: Sports
      value: sports
    - label: Culinary
      value: Culinary
---

