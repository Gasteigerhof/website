---
type: page
locale: en
view: offers
menu:
  - main-right
rank: 6

path: /en/offers
title: Offers
meta:
  title: Offers | 4* Gasteigerhof Stubaital
  description: Take advantage of our year-round special offers | Ski Opening | ski start | spring skiing | summer freshness | Golden autumn and much more. Room prices at special conditions incl. free glacier ski pass in winter

offers:
  intro:
    title: For those with an eye for a bargain.
    content: |-
      A holiday at the Gasteigerhof is always worth it – especially if you book one of our offers. Whether you are staying alone, as a couple or with the whole family, we have just the right package for every season. So you will always get all the benefits of being a guest here – just more of them, for less.
---
