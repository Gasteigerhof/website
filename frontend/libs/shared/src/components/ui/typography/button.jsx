import { Fragment, useMemo } from "react";
import PropTypes from "prop-types";

import { removeProperty } from "utils";
import { SeparatorIcon, StarIcon, ArrowIcon } from "icons";

import { Text } from "./text";

import * as styles from "./button.module.scss";

const STARS_COUNT = 4;

const THEME_FLAT = "theme_flat";
const THEME_STARS = "theme_stars";
const THEME_ARROW = "theme_arrow";
const THEME_ARROW_BACK = "theme_arrow_back";

const getDefaultSize = (theme, size) => {
  if (typeof size === "string") {
    return size;
  }

  switch (theme) {
    case THEME_FLAT:
      return "s4";
    default:
      return "s2";
  }
};

const getDefaultFamily = (theme, family) => {
  if (typeof family === "string") {
    return family;
  }

  switch (theme) {
    case THEME_FLAT:
      return "f1";
    default:
      return "f2";
  }
};

const Button = ({
  size,
  theme,
  family,
  transform,
  children,
  className,
  wrapper: Wrapper,
  ...props
}) => {
  size = getDefaultSize(theme, size);
  family = getDefaultFamily(theme, family);

  const hasIcons = theme !== THEME_FLAT;
  const wrapperClassName = useMemo(
    () => [styles.wrapper, styles[theme], className].filter(Boolean).join(" "),
    [theme, className]
  );

  return (
    <Wrapper className={wrapperClassName} {...props}>
      <Text
        size={size}
        family={family}
        wrapper="span"
        color="inherit"
        transform={transform}
        className={styles.text}
      >
        {children}
      </Text>
      {hasIcons && (
        <span className={styles.icons}>
          {theme === THEME_STARS && (
            <Fragment>
              <SeparatorIcon className={styles.separator} />
              <span className={styles.stars}>
                {Array.from({ length: STARS_COUNT }).map((_, key) => (
                  <StarIcon key={key} className={styles.star} />
                ))}
              </span>
              <SeparatorIcon className={styles.separator} />
            </Fragment>
          )}
          {(theme === THEME_ARROW || theme === THEME_ARROW_BACK) && (
            <span className={styles.arrows}>
              <ArrowIcon className={styles.arrow} />
            </span>
          )}
        </span>
      )}
    </Wrapper>
  );
};

Button.defaultProps = {
  size: null,
  family: null,
  transform: "uppercase",
  className: null,
  wrapper: "button",
  theme: THEME_ARROW,
  onClick: () => {},
};

Button.propTypes = {
  size: PropTypes.string,
  family: PropTypes.string,
  transform: PropTypes.string,
  className: PropTypes.string,
  wrapper: PropTypes.elementType,
  onClick: PropTypes.func,
  theme: PropTypes.oneOf([THEME_ARROW, THEME_ARROW_BACK, THEME_STARS, THEME_FLAT]).isRequired,
};

export const ButtonArrow = (props) => <Button {...props} theme={THEME_ARROW} />;
ButtonArrow.defaultProps = removeProperty(Button.defaultProps, "theme");
ButtonArrow.propTypes = removeProperty(Button.propTypes, "theme");

export const ButtonArrowBack = (props) => <Button {...props} theme={THEME_ARROW_BACK} />;
ButtonArrowBack.defaultProps = removeProperty(Button.defaultProps, "theme");
ButtonArrowBack.propTypes = removeProperty(Button.propTypes, "theme");

export const ButtonStars = (props) => <Button {...props} theme={THEME_STARS} />;
ButtonStars.defaultProps = removeProperty(Button.defaultProps, "theme");
ButtonStars.propTypes = removeProperty(Button.propTypes, "theme");

export const ButtonFlat = (props) => <Button {...props} theme={THEME_FLAT} />;
ButtonFlat.defaultProps = removeProperty(Button.defaultProps, "theme");
ButtonFlat.propTypes = removeProperty(Button.propTypes, "theme");
