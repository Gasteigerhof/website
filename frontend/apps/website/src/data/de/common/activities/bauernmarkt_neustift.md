---
type: page
locale: de
view: activity

path: /aktivitaeten-im-stubaital/bauernmarkt-neustift
title: "Liebe geht durch den Magen: Bauernmarkt Neustift"
meta:
  title: "Liebe geht durch den Magen: Bauernmarkt Neustift"
  description: Regionalität geht durch den Magen.

activity:
  date: 2022-06-21
  highlight: false
  hidden: false
  categories:
    - fruehling
    - sommer
    - herbst
    - kulinarik
    - dorfleben
  image: ./bauernmarkt-neustift/image.jpg
  content: |-
    Regionalität geht durch den Magen. Ob duftender Käse, Speck, edle Destillate oder heimischer Waldhonig. Ideal zum direkt vor Ort Genießen oder als Geschenk für Freunde und Familie zuhause.
  facts:
    - label: "Tag"
      value: "jeden Freitag"
    - label: "Uhrzeit"
      value: "ab 14 Uhr bis einschließl. September"
    - label: "Ort"
      value: "Musikpavillon Neustift"   

  gallery:
    - ./bauernmarkt-neustift/image.jpg
---
