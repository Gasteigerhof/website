---
type: page
locale: de
view: activity

path: /aktivitaeten-im-stubaital/schneeaktivitaet
title: "Ein Winter der Erlebnisse"
meta:
  title: "Ein Winter der Erlebnisse"
  description: Winteraktivitäten im Stubaital Tirol

activity:
  date: 2022-10-11
  highlight: false
  hidden: true
  categories:
    - winter
    - sport
    - kinder
  image: ./schneeaktivitaet/Stubaital-winter-aktivitaeten1.jpg
  content: |-
  
  facts:
    - label: "geeignet für"
      value: "die ganze Familie"
    - label: "Aktivitäten"
      value: "Langlaufen, Winterwandern, Eislaufen, Eisklettern, Schneeschuhwandern"
    - label: "Saison"
      value: "Winter"

  grid:
    texts:
      - |-
        Eislaufen: Drei Eislaufplätze im Stubaital bieten das perfekte Erlebnis auf Eis! Die Eisarena Fulpmes, der Eislaufplatz Neustift und der Natureislaufplatz Klause Auele haben täglich geöffnet. Auch für Eistockschießen!
      - |-
        Winterwandern: Auf 80 Kilometern geräumten und gut markierten Wegen können Sie Ihren Gedanken freien Lauf lassen. Ob mit Schneeschuhen oder mit Rodel im Tau. Es ist für alle was dabei.
      - |-
        Langlaufen: Direkt ab dem Gasteigerhof mit den Langlaufskiern in drei Kilometern, schneesicher nach Falbeson. Von dort aus können Sie aus einer Vielzahl an Routen – in jedem Schwierigkeitsgrad wählen.
      - |-
        Eisklettern: Reich an Wasserfällen. Das Stubaital verwandelt sich im Winter zum El-Dorado für Eiskletterer. Zahlreiche, faszinierende Eisgebilde warten darauf bestiegen zu werden.
    images:
      - ./schneeaktivitaet/Stubaital-winter-aktivitaeten1.jpg
      - ./schneeaktivitaet/Stubaital-winter-aktivitaeten2.jpg
      - ./schneeaktivitaet/Stubaital-winter-aktivitaeten3.jpg
      - ./schneeaktivitaet/Stubaital-winter-aktivitaeten4.jpg
---
