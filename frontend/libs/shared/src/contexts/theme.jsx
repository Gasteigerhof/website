import { createContext, useState, useMemo } from "react";
import PropTypes from "prop-types";

export const THEME_LIGHT = "light";
export const THEME_DARK = "dark";

const THEMES = {
  [THEME_LIGHT]: true,
  [THEME_DARK]: true,
};

export const ThemeProvider = ({ initialTheme, initialMeta, ...props }) => {
  const [theme, setTheme] = useState(initialTheme);
  const [meta, setMeta] = useState(initialMeta);

  const value = useMemo(
    () => ({
      meta,
      setMeta,
      theme,
      setTheme,
    }),
    [meta, theme]
  );

  return <ThemeContext.Provider value={value} {...props} />;
};

ThemeProvider.defaultProps = {
  initialTheme: THEME_LIGHT,
};

ThemeProvider.propTypes = {
  initialTheme: PropTypes.oneOf(Object.keys(THEMES)).isRequired,
};

export const ThemeContext = createContext();
