const path = require("path");

const SRC_PATH = path.resolve("src");

exports.onCreateWebpackConfig = ({ actions, getConfig }) => {
  const config = getConfig();

  // It's safe to disable import order warning for CSS Modules:
  // https://github.com/webpack-contrib/mini-css-extract-plugin?tab=readme-ov-file#remove-order-warnings
  // https://stackoverflow.com/questions/63124432/how-do-i-configure-mini-css-extract-plugin-in-gatsby
  const miniCssExtractPlugin = config.plugins.find(
    (plugin) => plugin?.constructor.name === `MiniCssExtractPlugin`
  );

  if (miniCssExtractPlugin) {
    miniCssExtractPlugin.options.ignoreOrder = true;
  }

  // https://www.gatsbyjs.com/docs/add-custom-webpack-config/
  config.resolve.modules = [SRC_PATH, "node_modules"];
  config.watchOptions = {
    poll: 1000,
    aggregateTimeout: 200,
    ignored: "**/node_modules",
  };

  actions.replaceWebpackConfig(config);
};
