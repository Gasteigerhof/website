---
type: page
locale: de
view: apartments
menu:
  - main-left
rank: 4

path: /wohnungen
title: Wohnungen
meta:
  title: Die Ferienwohnungen | 4* Gasteigerhof Stubaital
  description: Das Lärchenheim bietet Platz für vier Familien | Ferienwohnungen am Stubaier Gletscher mit eigener Küche und Platz für die ganze Familie.

apartments:
  intro:
    title: Unser Platz-Angebot.
    content: |-
      Unsere Gäste sollen alle den Platz bekommen, den Sie für Ihre Ruhe und 
      Entspannung brauchen. Manche mögen bekocht werden. Und manche mögen eben 
      auch mal selbst in Ruhe kochen. Manche genießen die Atmosphäre des 
      Gasteigerhof am Abend in der Lounge am Feuer.

      Und manche genießen die Ruhe und die Rückzugsmöglichkeit einer eigenen 
      Ferienwohnung für die ganze Familie.
    images:
      - ./apartments/intro.jpg
      - ./apartments/grid-bottom-right.jpg
      - ./apartments/grid-top-right.jpg      
  grid:
    images:
      top_left: ./apartments/grid-top-left.jpg
      top_right: ./apartments/grid-top-right.jpg
      bottom_left: ./apartments/grid-bottom-left.jpg
      bottom_right: ./apartments/grid-bottom-right.jpg
    texts:
      title: Jeder Topf hat den passenden Deckel.
      content: |-
        Weil Vorlieben bekanntlich unterschiedlich sind, bieten wir unseren Gästen 
        auch die Möglichkeit in unserem Lärchenheim Urlaub zu machen. Vier Ferienwohnungen 
        als Ihr persönlicher Rückzugsort. Und sollte der Ski- oder Wandertag doch mal zu 
        anstrengend gewesen sein, können Sie jederzeit in den Gasteigerhof kommen (nur 
        wenige Meter entfernt) und sich dort im Wellnessbereich erholen oder ihr Dinner 
        in unserem Restaurant genießen. Das Beste aus beiden Welten sozusagen.
      banner: Genügend Platz in unseren Badezimmern.

  map:
    image: ./apartments/map.svg

  info:
    title: "Alles und mehr."
    content: |-
      Manche mögen es lieber größer, privater und etwas ruhiger. Mit zwei getrennten Doppelzimmern pro Ferienwohnung jeweils mit Dusche/WC steht einem Familienurlaub nichts im Wege.
    lines:
      - label: Preis
        value: ab € 85,-
      - label: Größe
        value: 60 m²
      - label: Personen
        value: 2-4
      - label: Besonderheit
        value: HD-TV, ruhige, sonnige Lage, Balkon mit Gletscherblick, zwei Doppelzimmer mit Dusche/WC, gratis WLAN, Kinderbett und Hochstühle, vollausgestattte Küche, Safe, kostenloser Parkplatz, täglicher Brötchenservice, Bettwäsche, Handtücher, Spültabs sind vorhanden, Skiraum mit Schuhtrockner
    url: ''

  services:
    title: Inklusivleistungen und Angebote im Hotel für unsere Ferienwohnungs-Gäste
    labels:
      - Sauna und Wellnessbereich mit Hallenbad, Solegrotte uvm. inklusive
      - Frühstück, Jause und 5-Gang-Dinner auf Wunsch (gegen Aufpreis)
      - Massage, Bäder und Kosmetikbehandlung auf Wunsch (gegen Aufpreis)
      - Ermäßigung im hauseigenen Skiverleih
---
