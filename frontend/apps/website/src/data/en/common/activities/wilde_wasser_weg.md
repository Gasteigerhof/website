---
type: page
locale: en
view: activity
  
path: /en/activities-in-stubai/wild-water-trail
title: "From the glacier to the valley: Wilde-Wasser-Weg"
meta:
  title: "From the glacier to the valley: Wilde-Wasser-Weg"
  description: Water is life! And on the Wilde-Wasser-Weg you can feel the tremendous power up close.

activity:
  date: 2022-06-19
  highlight: false
  hidden: false
  categories:
    - Spring
    - Summer
    - Fall
    - Sports
    - Family
  image: ./wilde-wasser-weg/image.jpg
  content: |-
    Water is life! And on the Wilde-Wasser-Weg you can feel the tremendous power up close. In three stages, the path leads from glaciers, over thundering waterfalls to small meadows. Perfect for the whole family and in any weather.
  facts:
    - label: "stages"
      value: "3"
    - label: "suitable for:"
      value: "the whole family"
    - label: "Stage 1:"
      value: "1,5h, 3,5km, 120hm"
    - label: "Stage 2:"
      value: "2,5h, 4km, 660hm"    
    - label: "Stage 3:"
      value: "1,5h, 3km, 400hm"            

  gallery:
    - ./wilde-wasser-weg/image.jpg
---
