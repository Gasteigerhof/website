---
type: page
locale: de
view: spa
menu:
  - main-right
rank: 7

path: /spa
title: Spa
meta:
  title: Wellness | 4* Gasteigerhof Stubaital
  description: Großzügige Wellnessoase mit Hallenbad | großes Sauna-Angebot | Ruheräume | Bäder | Massagen | Kosmetik | Naturentspannung am Wasserfall | Der Gasteigerhof ist Rückzugsort für die ganze Familie

spa:
  intro:
    image: ./spa/intro.jpg
    title: Wellness, das weitergeht.
    content: |-
      Es gibt nicht viele Momente im Jahr, in denen man sich einfach treiben lassen kann.
      Wir finden ihr Aufenthalt im Gasteigerhof sollte so ein Moment sein.

      Denn selbst der Urlaub ist manchmal voll durchgeplant. Früh aufstehen, den ganzen
      Tag Skifahren oder Wandern, tag ein tag aus. Da sollten Sie es nicht verpassen sich
      Zeit für unseren Wellnessbereich zu nehmen. Denn wenn Sie das machen, dann machen Sie 
      aus Ihrer Zeit, echte Auszeit.
    images:
      - ./spa/intro.jpg
      - ./spa/Gasteigerhof_Spa.jpg
      - ./spa/Gasteigerhof_Spa_1.jpg
      - ./spa/Gasteigerhof_Spa_2.jpg
      - ./spa/Gasteigerhof_Spa_3.jpg
      - ./spa/Gasteigerhof_Spa_4.jpg
      - ./spa/Gasteigerhof_Spa_5.jpg  
      - ./spa/Gasteigerhof_Spa_6.jpg
      - ./spa/Gasteigerhof_Spa_7.jpg
      - ./spa/Gasteigerhof_Spa_8.jpg
      - ./spa/Gasteigerhof_Spa_9.jpg
      - ./spa/Gasteigerhof_Spa_10.jpg
      - ./spa/Gasteigerhof_Spa_11.jpg
      - ./spa/Gasteigerhof_Spa_12.jpg
      - ./spa/Gasteigerhof_Spa_13.jpg

  services:
    title: Wellness-Inklusivleistungen für unsere Gäste
    images:
      - ./common/images/hallenbad.svg
      - ./common/images/massage.svg
      - ./common/images/sauna.svg
    labels:
      - "Almsauna - Biosauna - Dampfbad"
      - "Ruheräume - Vitalcenter - Solegrotte - Fitnessstudio"
      - "Relax-Kabine - Heubetten - Quellwasserbrunnen"
      - "Farbdusche - Hallenbad - Liegewiesen"
      - "Vitalbar"
  grid:
    images:
      top_left: ./spa/Gasteigerhof_Spa_1.jpg
      top_right: ./spa/Gasteigerhof_Spa_6.jpg
      bottom_left: ./spa/Gasteigerhof_Spa_10.jpg
      bottom_right: ./spa/Gasteigerhof_Spa_13.jpg
    texts:
      title: Wenn Sie schon mal hier sind.
      content: |-
        Für vollständige Entspannung empfehlen wir Ihnen unsere Massagen, Bäder und Kosmetikangebote. 
        Unser bestens ausgebildetes Personal wird Sie verzaubern.

        Sie können spontan vor Ort an der Rezeption oder gerne vorab online dazubuchen. Online profitieren 
        Sie von unseren FrühbucherInnen-Rabatten.
      banner: Bei uns können Sie entspannen!
  
  offers:
    title: Entspannung liegt in unserer Natur.
    content: |-
      Diese Natur finden Sie unmittelbar vor unserer Haustüre. Und dort können Sie so richtig abschalten. Ob im Winter bei einer ruhigen Wanderung durch die Stubaier Schneelandschaft oder im Sommer auf einer Holzliege liegend beim angenehmen Rauschen des Wasserfalls.
  
      Fragen Sie gerne an der Rezeption für besondere Entspannungstipps in unserer Umgebung.
    image: ./spa/natur.jpg
    items:
      - label: Das gesamte Angebot im Überblick
        file: ./spa/gasteigerhof-wellness-angebot.pdf
---
