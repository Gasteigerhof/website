---
type: services
locale: de

services:
  title: Inklusivleistungen für unsere Gäste 
  images:
    - ./images/gratis-wlan.svg
    - ./images/hallenbad.svg
    - ./images/fruehstueck.svg
    - ./images/jause.svg
    - ./images/5-gaenge-menue.svg
    - ./images/kids-area.svg
    - ./images/bademaentel.svg
    - ./images/skiverleih.svg
  labels:
    - Gratis WLAN
    - Wellness, Pool & Sauna
    - Reichhaltiges Frühstück
    - Nachmittagsjause
    - 5-Gang Dinner
    - Großer Kinder-Spielebereich
    - frische Bademäntel & Handtücher
    - hauseigener Ski-Verleih
    - kostenfreie Stornierung bis 3 Wochen vor Anreise
---

