---
type: activity-categories
locale: de

activity_categories:
  items:
    - label: Alle
      value: null
    - label: Frühling
      value: fruehling
    - label: Sommer
      value: sommer
    - label: Herbst
      value: herbst
    - label: Winter
      value: winter
    - label: Familie
      value: familie
    - label: Gasteigerhof
      value: gasteigerhof
    - label: Dorfleben
      value: dorfleben
    - label: Kinder
      value: kinder
    - label: Sport
      value: sport
    - label: Kulinarik
      value: kulinarik
---

