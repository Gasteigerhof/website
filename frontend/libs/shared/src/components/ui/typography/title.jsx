import { useMemo } from "react";
import PropTypes from "prop-types";

import * as styles from "./title.module.scss";

export const Title = ({
  wrapper: Wrapper,
  size,
  color,
  family,
  weight,
  gradient,
  children,
  className,
  transform,
  truncate,
  ...props
}) => {
  const mappedSize = useMemo(() => {
    if (typeof size !== "undefined") {
      return size;
    }

    switch (Wrapper) {
      case "h1":
        return "s8";
      case "h2":
        return "s7";
      case "h3":
        return "s5";
      case "h4":
        return "s4";
      case "h5":
        return "s3";
      default:
        return "s2";
    }
  }, [size, Wrapper]);

  const wrapperClassName = useMemo(
    () =>
      [
        styles.wrapper,
        styles[color],
        styles[family],
        styles[weight],
        styles[Wrapper],
        styles[transform],
        styles[mappedSize],
        gradient ? styles.gradient : "",
        className,
      ]
        .filter(Boolean)
        .join(" "),
    [color, family, weight, transform, Wrapper, mappedSize, gradient, className]
  );

  const content = useMemo(
    () =>
      typeof children === "string" && typeof truncate === "number"
        ? children.length > truncate
          ? `${children.slice(0, truncate)}...`
          : children
        : children,
    [children, truncate]
  );

  return (
    <Wrapper className={wrapperClassName} {...props}>
      {content}
    </Wrapper>
  );
};

Title.defaultProps = {
  color: "inherit",
  transform: "inherit",
  wrapper: "h2",
  weight: "w7",
  family: "f1",
  className: null,
};

Title.propTypes = {
  size: PropTypes.oneOf(["s1", "s2", "s3", "s4", "s5", "s6", "s7", "s8", "s9", "s10"]),
  weight: PropTypes.oneOf(["w1", "w2", "w3", "w4", "w5", "w6", "w7"]),
  family: PropTypes.oneOf(["f1", "f2"]),
  color: PropTypes.oneOf(["inherit", "black", "white", "primary", "error"]),
  transform: PropTypes.oneOf(["inherit", "uppercase", "lowercase"]),
  wrapper: PropTypes.oneOf(["h1", "h2", "h3", "h4", "h5", "h6"]),
  truncate: PropTypes.number,
};
