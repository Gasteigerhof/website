import { useEffect, useState } from "react";

export const useKeyPress = (key) => {
  const [isKeyPressed, setIsKeyPressed] = useState(false);

  useEffect(() => {
    const downHandler = ({ key: currentKey }) => {
      if (key === currentKey) {
        setIsKeyPressed(true);
      }
    };

    const upHandler = ({ key: currentKey }) => {
      if (key === currentKey) {
        setIsKeyPressed(false);
      }
    };

    window.addEventListener("keydown", downHandler);
    window.addEventListener("keyup", upHandler);

    return () => {
      window.removeEventListener("keydown", downHandler);
      window.removeEventListener("keyup", upHandler);
    };
  }, [key]);

  return isKeyPressed;
};
