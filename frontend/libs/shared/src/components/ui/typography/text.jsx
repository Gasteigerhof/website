import { useMemo } from "react";
import PropTypes from "prop-types";

import { HTML } from "../../misc";

import * as styles from "./text.module.scss";

export const Text = ({
  wrapper: Wrapper,
  color,
  family,
  size,
  weight,
  children,
  transform,
  className,
  truncate,
  newLineToBreak,
  ...props
}) => {
  const wrapperClassName = useMemo(
    () =>
      [
        styles.wrapper,
        styles[size],
        styles[color],
        styles[transform],
        styles[weight],
        styles[family],
        className,
      ].join(" "),
    [size, color, transform, weight, family, className]
  );

  const content = useMemo(
    () =>
      typeof children === "string" && typeof truncate === "number"
        ? children.length > truncate
          ? `${children.slice(0, truncate)}...`
          : children
        : children,
    [children, truncate]
  );

  const processed = useMemo(
    () =>
      typeof content === "string" && newLineToBreak === true ? (
        <HTML content={content.replace(/(?:\r\n|\r|\n)/g, "<br>")} />
      ) : (
        content
      ),
    [content, newLineToBreak]
  );

  return (
    <Wrapper className={wrapperClassName} {...props}>
      {processed}
    </Wrapper>
  );
};

Text.defaultProps = {
  size: "s3",
  weight: "w4",
  family: "f1",
  color: "inherit",
  transform: "inherit",
  wrapper: "div",
  newLineToBreak: false,
  className: null,
};

Text.propTypes = {
  size: PropTypes.oneOf(["s1", "s2", "s3", "s4", "s5", "s6", "s7", "s8", "s9", "s10"]),
  weight: PropTypes.oneOf(["w1", "w2", "w3", "w4", "w5", "w6", "w7"]),
  family: PropTypes.oneOf(["f1", "f2", "f3"]),
  color: PropTypes.oneOf(["inherit", "black", "white", "primary", "error"]),
  transform: PropTypes.oneOf(["inherit", "uppercase", "lowercase"]),
  wrapper: PropTypes.oneOf(["div", "p", "span"]),
  truncate: PropTypes.number,
  newLineToBreak: PropTypes.bool,
  className: PropTypes.string,
};
