---
type: page
locale: en
view: request
menu:
  - main-right
rank: 9

path: /en/request
title: Request
meta:
  title: Anfrage | 4* Gasteigerhof Stubaital
  description: Send us a non-binding enquiry and we will send you a non-binding offer by return!

request:
  intro:
    title: Your enquiry
    content: |- 
      Send us an enquiry and we will send you a non-binding offer by return!

  form:
    labels:
      retry: "Send again"
      submit: "Inquire now"
      expander: "Please complete your data"
    sections:
      - title: "Information on your Stay"
        rows: 
          - fields:
            - name: arrival
              label: "Favoured arrival"
              type: DATE
              required: true
            - name: departure
              label: "Favoured depature"
              type: DATE
              required: true
            - name: room_type
              label: "Type of Room"
              type: SELECT
              required: true
              options:
                - "Double Room"
                - "Family Room"
                - "Suite Glacier View"
                - "Suite Zuckerhütl"
                - "Apartment Lärchenheim"
            expandable: false
          - fields:
            - name: adults
              label: "Number of Adults"
              type: NUMBER
              required: true
            - name: children
              label: "Number of Children"
              type: NUMBER
              required: false
            - name: children_age
              label: "Age Children"
              type: TEXT
              required: false
            expandable: false
      - title: "Your data"
        rows:
          - fields:
            - name: title
              label: "Salutation"
              type: SELECT
              required: false
              options:
                - Herr
                - Frau
                - no indication
                - Familie
                - Firma
            - name: forename
              label: "First name"
              type: TEXT
              required: false
            - name: surname
              label: "Surname"
              type: TEXT
              required: false
            - name: email
              label: "Email"
              type: EMAIL
              required: true
            expandable: false
          - fields:
            - name: street
              label: "Street"
              type: TEXT
              required: false
            - name: zip
              label: "Zip code"
              type: TEXT
              required: false
            - name: city
              label: "City"
              type: TEXT
              required: false
            expandable: true
          - fields:
            - name: country
              label: "Country"
              type: TEXT
              required: false
            - name: phone
              label: "Phone"
              type: TEXT
              required: false
            expandable: true
      - title: null
        rows:
          - fields:
            - name: message
              label: "Further requests"
              type: TEXTAREA
              required: false
            expandable: false
          - fields:
            - name: privacy
              label: "I have read the [terms of privacy](/en/privacy)"
              type: CHECKBOX
              required: true
            expandable: false
    success:
      title: Thank you for your inquiry!
      content: |-
        We will take care of it immediately and get in touch with you.
    failure:
      title: We are sorry...
      content: |-
        ...an error occurred while sending your request!
---
