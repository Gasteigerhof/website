---
type: page
locale: de
view: hotel
menu:
  - main-left
rank: 2

path: /hotel
title: Hotel
meta:
  title: Das Hotel | 4* Gasteigerhof Stubaital
  description: Der Gasteigerhof ist eine echte Familienangelegenheit. 45 Zimmer | umfangreiches Kulinarik-Angebot | großzügiger Wellness-Bereich | Suiten mit Gletscherblick

hotel:
  intro:
    title: Familien­angelegenheit, mit dem Stubaital im Herzen.
    content: |-
      Der Gasteigerhof ist eine echte Familienangelegenheit. Von Vater Hermann und Mutter Maria erbaut 
      und jahrzehntelang sorgfältig und liebevoll geführt und erweitert, wird der Gasteigerhof nun von 
      der zweiten Generation tatkräftig in die Zukunft geleitet. Matthias, Patrizia und Adrian Siller 
      sind aber nicht die Neuen im Haus. Sie kennen und lieben den Betrieb seit ihrer Kindheit. Und das 
      spürt man. Beim Umgang mit dem gesamten Team, beim immerwährenden Lächeln in ihren Gesichtern – 
      und vor allem bei der Fürsorge ihren Gästen gegenüber.

  about:
    title: Altbewährtes neu gedacht.
    content: |-
      Der Gasteigerhof ist inzwischen zu einer kleinen Institution gereift.
      
      Ein Hotel, dessen Namen weit über die Grenzen des kleinen Örtchen Gasteig und dem Stubaital hinaus bekannt ist.
      
      Das mag an der unmittelbaren Nähe zum Stubaier Gletscher, an den herrlichen Stubaier Almen und Wanderzielen oder an den spannenden Stubaier
      Naturschauplätzen liegen.
    image: ./hotel/about.jpg

  gallery:
    images:
      - ./hotel/gallerie-1.jpg
      - ./hotel/gallerie-2.jpg
      - ./hotel/gallerie-3.jpg
      - ./hotel/gallerie-4.jpg
      - ./hotel/Gasteigerhof-bar-01.jpg      

  slogan:
    title: Alles nur für unseren Gast.
    banner: Und das rund um die Uhr.

  team:
    title: Das schönste Hier und Jetzt.
    content: |-
      Ein kleines bisschen - und das werden Sie nach ihrem Aufenthalt bestätigen können - 
      darf sich auch die Familie Siller und ihr langjähriges Team zugute schreiben. Und 
      damit das so bleibt, entwickelt sich der Gasteigerhof stetig weiter – bleibt sich 
      aber immer treu, mit echter, ehrlicher Gastfreundschaft.
    image: ./hotel/team.jpg

  kids:
    title: Kinder sind bei uns daheim.
    content: |-
      Der Gasteigerhof im Stubaital war schon immer ein Familienhotel. Nicht nur familiengeführt, sondern auch stets für Familien da. Ihre Kinder sind bei uns herzlichst willkommen. Deshalb haben wir auch extra das "Hamsterland" im Gasteigerhof eingerichtet. Mit Bällebad, Kletterwand, lustigen Spielen uvm. Zudem dürfen Kinder bei uns auch in das Hallenbad (mit Vulkansprudel). Im Außenbereich steht ein Abenteuerspielplatz zur Verfügung und wenn Sie für ihr Zimmer besondere Einrichtung (zb. Gitterbett) benötigen, dann geben Sie uns gerne an der Rezeption bescheid. Mit der Stubai-Super-Card und dem Big Family Programm ist im gesamten Stubaital für Familienangebot gesorgt. Oben drauf erhalten Kinder unter 10 Jahren bei uns einen gratis Skipass und 10% Ermäßigung im hauseigenen Skiverleih.
    image: ./hotel/kids.jpg

  offers:
    title: Unsere Angebote
    items:
      - label: 45 Zimmer
        image: ./common/images/45-zimmer.svg
      - label: Restaurant
        image: ./common/images/restaurant.svg
      - label: Hallenbad
        image: ./common/images/hallenbad.svg
      - label: Frühstück
        image: ./common/images/fruehstueck.svg
      - label: 5-Gang-Menü
        image: ./common/images/5-gaenge-menue.svg
      - label: Jause
        image: ./common/images/jause.svg
      - label: Massage
        image: ./common/images/massage.svg
      - label: Sauna
        image: ./common/images/sauna.svg
      - label: Fitness
        image: ./common/images/fitness.svg
      - label: Kids-Area
        image: ./common/images/kids-area.svg
      - label: Gratis WLAN
        image: ./common/images/gratis-wlan.svg
      - label: Gletscherblick
        image: ./common/images/gletscherblick.svg
      - label: Wasserfall
        image: ./common/images/wasserfall.svg
      - label: Wanderungen
        image: ./common/images/wanderungen.svg
      - label: Skiverleih
        image: ./common/images/skirent.svg
      - label: Skifahren
        image: ./common/images/skifahren.svg
      - label: Langlaufen
        image: ./common/images/langlauf.svg

---
