import { Fragment } from "react";
import PropTypes from "prop-types";
import { GatsbyImage } from "gatsby-plugin-image";

import { Section, getProperty, Title, Text, Markdown } from "@gh/shared";

import { ImageCardRight } from "../common/image-card";

export const Kids = ({ data }) => {
  const title = getProperty(data, "title");
  const content = getProperty(data, "content");
  const image = getProperty(data, "image.childImageSharp.gatsbyImageData");

  return (
    <Section>
      <ImageCardRight
        fullScreen={true}
        content={
          <Fragment>
            <Title>{title}</Title>
            <Text>
              <Markdown content={content} />
            </Text>
          </Fragment>
        }
        image={<GatsbyImage image={image} alt="" />}
      />
    </Section>
  );
};

Kids.defaultProps = {
  data: {},
};

Kids.propTypes = {
  data: PropTypes.shape({
    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    image: PropTypes.object.isRequired,
  }).isRequired,
};
