---
type: page
locale: de
view: cuisine
menu:
  - main-left
rank: 5

path: /kulinarik
title: Kulinarik
meta:
  title: Kulinarik | 4* Gasteigerhof Stubaital
  description: Tägliches Verwöhnfrühstück mit regionalen Produkten, teils aus eigener Produktion | Nachmittagsjause mit Kaffee und Kuchen | Kindermenü und Eis vom Buffet | 5-Gang Dinner mit regionalen Köstlichkeiten

cuisine:
  intro:
    image: ./cuisine/intro.jpg
    title: Ehrlich gut.
    content: |-
      Das versprechen viele. Und das ist auch gut so. Denn je mehr diesem Versprechen 
      nachkommen, desto besser für uns alle. Desto mehr müssen wir uns aber auch anstrengen, dass man im Gasteigerhof nach wie vor den Unterschied schmeckt.

  grid:
    images:
      top_left: ./cuisine/grid-top-left.jpg
      top_right: ./cuisine/grid-top-right.jpg
      bottom_left: ./cuisine/grid-bottom-left.jpg
      bottom_right: ./cuisine/grid-bottom-right.jpg
    texts:
      title: Zutaten und Produkte im Gasteigerhof.
      content: |-
        Dem Anspruch von höchster Qualität stellen wir uns gerne. Sei es beim Brot das frisch von der lokalen 
        Bäckerei kommt oder bei unseren Rindern, die im Sommer auf den Stubaier Almen grasen. 
        Denn einen Schritt weiter zu sein, heißt für uns in der Region zu bleiben. Ganz einfach.
      banner: Liebe geht durch den Magen.

  services:
    title: Kulinarische Inklusivleistungen für unsere Gäste
    labels:
      - Reichhaltiges Frühstücksbuffet
      - Nachmittagsjause mit Produkten aus der Region und
      - Hausgemachte Kuchen mit Kaffee und Tee
      - 5-Gang-Dinner – natürlich auch vegetarisch
      - Begleitendes Salat- und Käsebuffet
      - Kindermenü mit Eis vom Buffet
      - Spezielle Diatwünsche auf Anfrage
      - Tee- und frisches Obst im Wellnessbereich
---
