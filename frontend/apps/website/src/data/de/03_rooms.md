---
type: page
locale: de
view: rooms
menu:
  - main-left
rank: 3

path: /zimmer
title: Zimmer
meta:
  title: Die Zimmer | 4* Gasteigerhof Stubaital
  description: Vier Zimmerkategorien | Doppelzimmer | Familienzimmer | Suite Gletscherblick | Suite Zuckerhütl | Alle Zimmer mit Balkon | Free WIFI | geräumiges Bad

rooms:
  intro:
    title: Schlaf gut. Bei uns im Gasteigerhof.
    content: |-
      In den 45 Zimmern des Gasteigerhof sorgen wir für Ihre Ruhe und Erholung.
      In vier unterschiedlichen Kategorien finden Sie das perfekte Zimmer zum Verweilen,
      Entspannen, sich Zurückziehen. Oder einfach nur zum lange Schlafen. Übrigens: All 
      unsere Zimmer verfügen über einen privaten Balkon.

  items:
    - id: doppelzimmer
      menu:
        title: Doppelzimmer
        subtitle: bereits ab
        price: € 100,-
      info:
        title: Zeit zu zweit.
        content: |-
          Unsere Doppelzimmer sind voll ausgestattet mit alpinem Flair und 
          großzügigem Freibereich. Und wie für unseren Gasteigerhof üblich ähnelt kein 
          Zimmer dem anderen.
        lines:
          - label: Preis
            value: ab € 100,-
          - label: Größe
            value: 20 m²
          - label: Personen
            value: '2'
          - label: Besonderheit
            value: Dusche oder Badewanne, Balkon und TV, Safe
        images:
          - ./rooms/01_doppelzimmer.jpg
          - ./rooms/02_doppelzimmer.jpg
          - ./rooms/03_doppelzimmer.jpg          
        url: ''

    - id: familienzimmer
      menu:
        title: Familienzimmer
        subtitle: bereits ab
        price: € 105,-
      info:
        title: Familiensache.
        content: |-
          Sie reisen als Familie an? Wir freuen uns! In unseren Familienzimmern finden Sie 
          extra viel Platz und Privatsphäre für Sie und Ihr(e) Kind(er). Im 
          Wohnbereich bleibt auch genügend Raum zum Spielen oder um am Abend etwas Ruhe 
          zu genießen.
        lines:
          - label: Preis
            value: ab € 105,-
          - label: Größe
            value: 30-35 m²
          - label: Personen
            value: 2 - 4
          - label: Besonderheit
            value: Wohnraum, Dusche, Balkon, HD-TV, Safe
        images:
          - ./rooms/01_familienzimmer.jpg
          - ./rooms/02_familienzimmer.jpg
          - ./rooms/03_familienzimmer.jpg
          - ./rooms/04_familienzimmer.jpg          
        url: ''

    - id: suite-gletscherblick
      menu:
        title: Suite Gletscherblick
        subtitle: bereits ab
        price: € 110,-
      info:
        title: Der Gletscherblick.
        content: |-
          Darf es etwas mehr sein? Die Gletscherblick-Suite überzeugt mit echtem Kachelofen, Echtholzboden,
           Wohnraum, separatem WC und geräumigem Bad mit Badewanne. Ach ja: der sonnige Südbalkon mit Gletscherblick 
          ist inklusive. So geht Urlaub.
        lines:
          - label: Preis
            value: ab € 110,-
          - label: Größe
            value: 40 m²
          - label: Personen
            value: 2 - 4
          - label: Besonderheit
            value: Südbalkon mit Gletscherblick, Wohnraum, Kachelofen, HD-TV, Echtholz, Badewanne
        images:
          - ./rooms/suite-gletscherblick.jpg
          - ./rooms/02_suit_gb.jpg
          - ./rooms/03_suit_gb.jpg                  
        url: ''

    - id: suite-zuckerhuetl
      menu:
        title: Suite Zuckerhütl
        subtitle: bereits ab
        price: € 115,-
      info:
        title: Zimmer mit Mehrblick.
        content: |-
          Mehr Sonne, mehr Berge, mehr Entspannung. Die Zuckerhütl-Suite überzeugt unglaublichem 
          Raumgefühl, mit einem abgetrennten Wohnraum, echtem Kachelofen, Holzboden, separatem WC und geräumigem 
          Bad mit Dusche. Ach ja: der große, sonnige Südbalkon mit Gletscherblick ist inklusive.
        lines:
          - label: Preis
            value: ab € 115,-
          - label: Größe
            value: 40 m²
          - label: Personen
            value: 2 - 4
          - label: Besonderheit
            value: Südbalkon mit Gletscherblick, Wohnraum, Kachelofen, HD-TV, Echtholz, Dusche
        images:
          - ./rooms/01_suit_zh.jpg
          - ./rooms/02_suit_zh.jpg
          - ./rooms/03_suit_zh.jpg
          - ./rooms/04_suit_zh.jpg
          - ./rooms/05_suit_zh.jpg
        url: ''

  pricelists:
    items: []
#      - file: ./rooms/preisliste-sommer.pdf
#        text: Preisliste Sommer
#      - file: ./rooms/preisliste-winter.pdf
#        text: Preisliste Winter

  annulation:
    title: Stornobedingungen
    content: ''
    items:
      - label: bis 3 Wochen
        value: keine Stornogebühren
      - label: 3 bis 1 Woche(n)
        value: 70% vom Gesamtpreis
      - label: in der letzten Woche
        value: 90% vom Gesamtpreis
    link:
      text: Mehr Infos
      path: /agb/#storno
---
